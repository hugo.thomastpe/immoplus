<?php

class ControllerPartenaire{
    function partenaire(){
        $p = new Partenaire();
        $partenaires = $p->getPartenaire();
        $partNom = $p->getPartAlpha();
        require("View/partenaire.php");
    }
    
    function ajouterPart(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $p = new Partenaire();
            $daoP = new DAOPartenaire();
            if($u->isAdmin($_SESSION['login'])){
    
                //Pour le formulaire
                $data = [
                    'idP' => '',
                    'nom' => '',
                    'logo' => '',
                    'lien' => '',
                    'idPError' => '',
                    'nomError' => '',
                    'logoError' => '',
                    'lienError' => ''
                ];
            
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    // Process form
                    // Sanitize POST data
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
                      $data = [
                        'idP' => trim($_POST['idP']),
                        'nom' => trim($_POST['nom']),
                        'logo' => trim($_POST['logo']),
                        'lien' => trim($_POST['lien']),
                        'idPError' => '',
                        'nomError' => '',
                        'logoError' => '',
                        'lienError' => ''
                    ];
            
                    $textValidation = "/^[a-zA-Z0-9\é\è\à\ç\.\:\!\?\%\/\ \,\'\'\(\)\-\_]*$/";
                    $digitValidation = "/^[0-9\.]*$/";
            
                    //Validate idC
                    if (empty($data['idP'])) { //Vérification si la zone de texte n'est pas vide
                        $data['idP'] = "Entrez une valeur pour l'id du partenaire";
                    } elseif (!preg_match($textValidation, $data['idP'])) { //Vérification si les caractères entrés par l'utilisateur sont corrects
                        $data['idPError'] = "Erreur sur l'id du partenaire', veuillez revoir la syntaxe.";
                    }
            
                    //Validate nom
                    if (empty($data['nom'])) {
                        $data['nom'] = 'Entrez une valeur pour le nom.';
                    } elseif (!preg_match($textValidation, $data['nom'])) {
                        $data['nomError'] = 'Erreur sur le nom, veuillez revoir la syntaxe.';
                    }
            
                    //Validate logo
                    if (empty($data['logo'])) {
                        $data['logo'] = 'Entrez une valeur pour le logo.';
                    } elseif (!preg_match($textValidation, $data['logo'])) {
                        $data['logoError'] = 'Erreur sur le logo, veuillez revoir la syntaxe.';
                    }
                    
            
                   // Validate lien
                    if(empty($data['lien'])){
                      $data['lien'] = 'Entrez une valeur pour la lien.';
                    } elseif (!preg_match($textValidation, $data['lien'])) {
                      $data['lienError'] = 'Erreur sur la lien, veuillez revoir la syntaxe.';
                    }
            
    
                    //S'il n'y a pas d'erreur de formulaire
                    if (empty($data['idPError']) && empty($data['nomError']) && empty($data['logoError']) && empty($data['lienError'])) {
    
    
                        $p->create($data['idP'], $data['nom'], $data['logo'], $data['lien']);
                        $daoP->create($p);
    
                        //Ajoute un partenaire et vérifie si la requête s'est bien exécutée
                        if ($daoP->addPart()) {
                            //Redirection à la page du bien modifié
                            header('location: index.php?action=partenaire');
                        } else {
                            die('Une erreur est survenue.');
                        }
                    }
                }
    
    
                require('View/addForm/ajouterPartenaire.php');
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
    
    function supprimerPart(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $p = new Partenaire();
            $daoP = new DAOPartenaire();
            if($u->isAdmin($_SESSION['login'])){
                $part = $p->getInfoPart($_GET['idp']);
                $lePart = $part->fetch();
                $p->create($lePart['idp'], $lePart['nom'], $lePart['logo'], $lePart['lien']);
                $daoP->create($p);
                if($daoP->deletePart()){
                    header("location: index.php?action=partenaire");
                }
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
}



?>