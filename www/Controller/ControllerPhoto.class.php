<?php

class ControllerPhoto{
    function addPhoto(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $b = new Bien();
            $p = new Photo();
            $daoP = new DAOPhoto();
            $infoBien = $b->getBien($_GET['idb']);
            if($u->isAdmin($_SESSION['login'])){
    
                    $data = [
                    'idphoto' => '',
                    'nom' => '',
                    'lien' => '',
                    'idPhotoError' => '',
                    'nomError' => '',
                    'lienError' => '',
                    ];
        
                    if($_SERVER['REQUEST_METHOD'] == 'POST'){
                        // Process form
                        // Sanitize POST data
                        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
                        $data = [
                            'idphoto' => trim($_POST['idphoto']),
                            'nom' => trim($_POST['nom']),
                            'lien' => trim($_POST['lien']),
                            'idPhotoError' => '',
                            'nomError' => '',
                            'lienError' => '',
                        ];
        
                        $textValidation = "/^[a-zA-Z0-9\é\è\à\ç\.\:\!\?\%\/\ \,\'\(\)\-\_]*$/";
        
                        
        
                        //Validate login on letters/numbers
                        if (empty($data['idphoto'])) {
                            $data['idPhotoError'] = 'Entrez un id valide.';
                        } elseif (!preg_match($textValidation, $data['idphoto'])) {
                            $data['idPhotoError'] = "Veuillez revoir la syntaxe de l'id.";
                        } else {
                            //Check if login exists.
                        if (!$p->findPhotoById($data['idphoto'])) {
                            $data['idPhotoError'] = 'Id déjà pris.';
                        }
                    }
        
                    //Validate name
                    if (empty($data['nom'])) {
                        $data['nomError'] = 'Entrez un nom.';
                    } elseif (!preg_match($textValidation, $data['nom'])) {
                        $data['nomError'] = 'Entrez un nom au bon format.';
                    }
        
                    //Validate firstname
                    if (empty($data['lien'])) {
                        $data['lienError'] = 'Entrez un prenom.';
                    } elseif (!preg_match($textValidation, $data['lien'])) {
                        $data['lienError'] = 'Entrez un prenom au bon format.';
                    }
        
                    // Make sure that errors are empty
                    if (empty($data['idPhotoError']) && empty($data['nomError']) && empty($data['LienError'])) {
        
    
                        $p->create($_GET['idb'], $data['idphoto'],$data['nom'], $data['lien']);
                        $daoP->create($p);
    
                        //Ajoute une image et vérifie si la requête s'est bien exécutée
                        if ($daoP->addPhoto()) {
                            header('location: index.php?action=bien&idb='.$_GET['idb']);
                        } else {
                            die('Une erreur est survenue.');
                        }
                    }
                }
                //print_r($data);
                require("View/addForm/ajouterPhoto.php");
        
            }else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    
    }
    
    function supprimerPhoto(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $p = new Photo();
            $daoP = new DAOPhoto();
            if($u->isAdmin($_SESSION['login'])){
                $photo = $p->getInfoPhoto($_GET['idb'], $_GET['idphoto']);
                $laPhoto = $photo->fetch();
                $p->create($laPhoto['idb'], $laPhoto['idphoto'], $laPhoto['nom'], $laPhoto['lien']);
                $daoP->create($p);
                if($daoP->deletePhoto()){
                    header("location: index.php?action=bien&idb=".$_GET['idb']);
                }
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
}



?>