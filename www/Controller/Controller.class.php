<?php


require('Model/Bien.class.php');
require("DAO/DAOBien.class.php");

require("Model/Photo.class.php");
require("DAO/DAOPhoto.class.php");

require("Model/Utilisateur.class.php");
require("DAO/DAOUtilisateur.class.php");

require("Model/Panier.class.php");
require("DAO/DAOPanier.class.php");

require("Model/Collaborateur.class.php");
require("DAO/DAOCollaborateur.class.php");

require("Model/Partenaire.class.php");
require("DAO/DAOPartenaire.class.php");



class Controller{
    function error(){
        require("View/error/error.php");
    }
    
    function catalogue()
    {
        $b=new Bien();
        $biens = $b->getBiens();
    
        require("View/catalogue.php");
    }
    
    
    function accueil()
    {
        $b=new Bien();
        $bienpop = $b->getBienPop();
        $bienrecent = $b->getBienRecent();
        require('View/accueil.php');
    
    }

    function agence(){
        $b = new Bien();
        $c = new Collaborateur();
        $p = new Partenaire();
        $u = new Utilisateur();

        $nbBiens = $b->getNbBiens();
        $nbCollab = $c->getNbCollab();
        $nbPart = $p->getNbPart();
        $nbUtili = $u->getNbUtili();

        require("View/agence.php");
    }
    
    function contact(){
        require("View/contact.php");
    }
    
    function rechercher(){
            echo "test";
            //Ici on récupère le cp de la localisation
            $cp = preg_replace('/[^0-9]/', '', $_POST['localite']);
    
            $bienR = new Bien();
            $biensRech = $bienR->rechercherBien($_POST['typSearch'], $_POST['typBien'], $cp, $_POST['prixMin'], $_POST['prixMax']);
            //header("location: index.php?action=catalogue");
    }
    
    
    
    function inscription() {
        $data = [
            'login' => '',
            'nom' => '',
            'prenom' => '',
            'password' => '',
            'confirmPassword' => '',
            'loginError' => '',
            'nomError' => '',
            'prenomError' => '',
            'passwordError' => '',
            'confirmPasswordError' => ''
        ];
    
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // Process form
        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
              $data = [
                'login' => trim($_POST['login']),
                'nom' => trim($_POST['nom']),
                'prenom' => trim($_POST['prenom']),
                'password' => trim($_POST['password']),
                'confirmPassword' => trim($_POST['confirmPassword']),
                'loginError' => '',
                'nomError' => '',
                'prenomError' => '',
                'passwordError' => '',
                'confirmPasswordError' => ''
            ];
    
            $loginValidation = "/^[a-zA-Z0-9]*$/";
            $nameValidation = "/^[a-zA-Z]*$/";
            $passwordValidation = "/^(.{0,7}|[^a-z]*|[^\d]*)$/i";
    
            $utilisateurTest = new Utilisateur();
    
            //Validate login on letters/numbers
            if (empty($data['login'])) {
                $data['loginError'] = 'Entrez un login valide.';
            } elseif (!preg_match($loginValidation, $data['login'])) {
                $data['loginError'] = 'Les Login ne peuvent contenir que des lettres et des chiffres.';
            } else {
                //Check if login exists.
                if (!$utilisateurTest->findUserByLogin($data['login'])) {
                    $data['loginError'] = 'Login déjà pris.';
                }
            }
    
            //Validate name
            if (empty($data['nom'])) {
                $data['nomError'] = 'Entrez un nom.';
            } elseif (!preg_match($nameValidation, $data['nom'])) {
                $data['nomError'] = 'Entrez un nom au bon format (minuscule ou majuscule).';
            }
    
            //Validate firstname
            if (empty($data['prenom'])) {
                $data['prenomError'] = 'Entrez un prenom.';
            } elseif (!preg_match($nameValidation, $data['prenom'])) {
                $data['prenomError'] = 'Entrez un prenom au bon format (minuscule ou majuscule).';
            }
            
    
           // Validate password on length, numeric values,
            if(empty($data['password'])){
              $data['passwordError'] = 'Entrez un mot de passe.';
            } elseif(strlen($data['password']) < 6){
              $data['passwordError'] = 'Le mot de passe doit avoir au moins huit caractères';
            } elseif (preg_match($passwordValidation, $data['password'])) {
              $data['passwordError'] = 'Le mot de passe nécessite au moins un chiffre.';
            }
    
            //Validate confirm password
            if (empty($data['confirmPassword'])) {
                $data['confirmPasswordError'] = 'Please enter password.';
            } else {
                if ($data['password'] != $data['confirmPassword']) {
                $data['confirmPasswordError'] = 'Passwords do not match, please try again.';
                }
            }
    
            // Make sure that errors are empty
            if (empty($data['loginError']) && empty($data['nomError']) && empty($data['prenomError']) && empty($data['passwordError']) && empty($data['confirmPasswordError'])) {
    
                // Hash password -> Pas besoin de le hasher car on le hash dejà dans la fonction insert() de la classe utilisateur
                //$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
    
                //Register user from model function
                echo $data['prenom']."et".$data['nom'];
                $utilisateurTest->create($data['login'],$data['password'], $data['nom'], $data['prenom'] );
                //print_r($utilisateurTest);
                //echo $utilisateurTest->getPrenom();
                if ($utilisateurTest->insert()) {
                    //Redirect to the login page
                    header('location: index.php?action=connexion');
                } else {
                    die('Une erreur est survenue.');
                }
            }
        }
        //print_r($data);
        require("View/inscription.php");
    }
    
    
    function connexion() {
        $data = [
            'login' => '',
            'password' => '',
            'loginError' => '',
            'passwordError' => ''
        ];
        $utilisateurTest = new Utilisateur();
    
        //Check for post
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            //Sanitize post data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
            $data = [
                'login' => trim($_POST['login']),
                'password' => trim($_POST['password']),
                'loginError' => '',
                'passwordError' => '',
            ];
            //Validate username
            if (empty($data['login'])) {
                $data['loginError'] = 'Entrez un Login.';
            }
    
            //Validate password
            if (empty($data['password'])) {
                $data['passwordError'] = 'Entrez un Mot de passe.';
            }
    
            //Check if all errors are empty
            if (empty($data['loginError']) && empty($data['passwordError'])) { //Si aucun champ n'est vide
                if(!$utilisateurTest->findUserByLogin($data['login'])){ //Si le nom d'utilisateur est utilisé
                    $loggedInUser = $utilisateurTest->seConnecter($data['login'], $data['password']); //On récupère les données de l'utilisateur (si il y en a)
                    $estAdmin = $utilisateurTest->isAdmin($data['login']); //On vérifie s'il est admin
                    if ($loggedInUser) { //Si il y a bien des données d'utilisateur
                    
                        $this->createUserSession($data['login'], $estAdmin); //On crée une session utilisateur
                    } else { //Sinon
                        print_r($loggedInUser);
                        $data['passwordError'] = 'Mot de passe ou Login incorrect. Essayez à nouveau.'; //On affiche un message d'erreur
    
                        //$this->view('users/login', $data);
                    }
                }else{ //Si le login n'existe pas
                    $data['passwordError'] = 'Mot de passe ou Login incorrect. Essayez à nouveau.'; //On affiche un message d'erreur
                }
                
            }
    
        } else {
            $data = [
                'login' => '',
                'password' => '',
                'loginError' => '',
                'passwordError' => ''
            ];
        }
        require("View/connexion.php");
    }
    
    function createUserSession($login, $admin) { //On rempli les variables de session
        $_SESSION['login'] = $login; 
        $_SESSION['admin'] = $admin;
        //$_SESSION['email'] = $user->email;
        header('location: index.php'); // On redirige vers l'index
    }
    
    function deconnexion() { // On détruit les variables de session
        unset($_SESSION['login']);
        unset($_SESSION['admin']);
        //unset($_SESSION['email']);
        session_destroy();
        header('location: index.php');
    }
}





?>