<?php

class ControllerBien{
    function bien(){
        $b=new Bien();
        $p=new Photo();
        $bien = $b->getBien($_GET['idb']);
        $photos = $p->getPhotosBien($_GET['idb']);
        require('View/bien.php');
    
    }
    
    function ajouterBien(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $b = new Bien();
            $daoB = new DAOBien();
            if($u->isAdmin($_SESSION['login'])){
    
                //Pour le formulaire
                $data = [
                    'typer' => '',
                    'typeb' => '',
                    'prix' => '',
                    'localisation' => '',
                    'description' => '',
                    'typeRError' => '',
                    'typeBError' => '',
                    'prixError' => '',
                    'localisationError' => '',
                    'descriptionError' => ''
                ];
            
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    // Process form
                    // Sanitize POST data
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
                      $data = [
                        'typer' => trim($_POST['typer']),
                        'typeb' => trim($_POST['typeb']),
                        'prix' => trim($_POST['prix']),
                        'localisation' => trim($_POST['localisation']),
                        'description' => trim($_POST['description']),
                        'typeRError' => '',
                        'typeBError' => '',
                        'prixError' => '',
                        'localisationError' => '',
                        'descriptionError' => ''
                    ];
            
                    $textValidation = "/^[a-zA-Z0-9\é\è\à\ç\.\:\!\?\%\/\ \,\'\(\)\-\_]*$/";
                    $digitValidation = "/^[0-9\.]*$/";
            
                    //Validate typeR
                    if (empty($data['typer'])) { //Vérification si la zone de texte n'est pas vide
                        $data['typer'] = 'Entrez une valeur pour le type de recherche.';
                    } elseif (!preg_match($textValidation, $data['typer'])) { //Vérification si les caractères entrés par l'utilisateur sont corrects
                        $data['typeRError'] = 'Erreur sur le type de recherche, veuillez revoir la syntaxe.';
                    } else if (($data['typer'] !== "Vente") && ($data['typer'] !== "Location") && ($data['typer'] !== "Autre")){ //Vérification si la valeur entrées par l'utilisateur est correcte
                        $data['typeRError'] = "La valeur entrée doit être 'Vente', 'Location' ou 'Autre'.";   
                    }
            
                    //Validate typeB
                    if (empty($data['typeb'])) {
                        $data['typeb'] = 'Entrez une valeur pour le type de bien.';
                    } elseif (!preg_match($textValidation, $data['typeb'])) {
                        $data['typeBError'] = 'Erreur sur le type de bien, veuillez revoir la syntaxe.';
                    } else if (($data['typeb'] !== "Maison") && ($data['typeb'] !== "Appartement") && ($data['typeb'] !== "Bureau") && ($data['typeb'] !== "Terrain") && ($data['typeb'] !== "Garage") && ($data['typeb'] !== "Autre")){ //Vérification si la valeur entrées par l'utilisateur est correcte
                        $data['typeBError'] = "La valeur entrée doit être 'Maison', 'Appartement' ou 'Bureau' ou 'Terrain' ou 'Garage' ou 'Autre'.";   
                    }
            
                    //Validate prix
                    if (empty($data['prix'])) {
                        $data['prix'] = 'Entrez une valeur pour le prix.';
                    } elseif (!preg_match($digitValidation, $data['prix'])) {
                        $data['prixError'] = 'Erreur sur le prix, veuillez revoir la syntaxe.';
                    }
                    
            
                   // Validate localisation
                    if(empty($data['localisation'])){
                      $data['localisation'] = 'Entrez une valeur pour la localisation.';
                    } elseif (!preg_match($textValidation, $data['localisation'])) {
                      $data['localisationError'] = 'Erreur sur la localisation, veuillez revoir la syntaxe.';
                    } elseif (!preg_match('/[a-zA-Z0-9\.\ \,\-_].*[0-9]|[0-9].*[a-zA-Z0-9\.\ \,\-_]/', $data['localisation'])) {
                        $data['localisationError'] = 'Erreur sur la localisation, il faut au moins une ville et le code postal.';
                    }
            
                    //Validate description
                    if(empty($data['description'])){
                        $data['description'] = 'Entrez une valeur pour la description.';
                    } elseif (!preg_match($textValidation, $data['description'])) {
                        $data['descriptionError'] = 'Erreur sur la description, veuillez revoir la syntaxe.';
                    }
    
                    //S'il n'y a pas d'erreur de formulaire
                    if (empty($data['typeRError']) && empty($data['typeBError']) && empty($data['prixError']) && empty($data['localisationError']) && empty($data['descriptionError'])) {
    
                        $b->create($data['typer'], $data['typeb'], $data['prix'], $data['localisation'], $data['description']);
                        $daoB->create($b);
                        //Modifier le bien depuis le model Bien
                        //if ($b->addBien($data['typer'],$data['typeb'], $data['prix'], $data['localisation'], $data['description'] )) {
                        if ($daoB->addBien()) {
                            //Redirection à la page du bien modifié
                            header('location: index.php?action=catalogue');
                        } else {
                            die('Une erreur est survenue.');
                        }
                    }
                }
    
    
                require('View/addForm/ajouterBien.php');
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
    
    function modifierBien(){
        //header("location: index.php?action=utilisateurs");
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $b = new Bien();
            $bienModif = $b->getBien($_GET['idb']);
            if($u->isAdmin($_SESSION['login'])){
    
                //Pour le formulaire
                $data = [
                    'typer' => '',
                    'typeb' => '',
                    'prix' => '',
                    'localisation' => '',
                    'description' => '',
                    'typeRError' => '',
                    'typeBError' => '',
                    'prixError' => '',
                    'localisationError' => '',
                    'descriptionError' => ''
                ];
            
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Process form
                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
                      $data = [
                        'typer' => trim($_POST['typer']),
                        'typeb' => trim($_POST['typeb']),
                        'prix' => trim($_POST['prix']),
                        'localisation' => trim($_POST['localisation']),
                        'description' => trim($_POST['description']),
                        'typeRError' => '',
                        'typeBError' => '',
                        'prixError' => '',
                        'localisationError' => '',
                        'descriptionError' => ''
                    ];
            
                    $textValidation = "/^[a-zA-Z0-9\é\è\à\ç\.\:\!\?\%\/\ \,\'\(\)\-\_]*$/";
                    $digitValidation = "/^[0-9\.]*$/";
            
                    //Validate typeR
                    if (empty($data['typer'])) {
                        $data['typer'] = $bienModif['typer'];
                    } elseif (!preg_match($textValidation, $data['typer'])) {
                        $data['typeRError'] = 'Erreur sur le type de recherche, veuillez revoir la syntaxe.';
                    } else if (($data['typer'] !== "Vente") && ($data['typer'] !== "Location") && ($data['typer'] !== "Autre")){ //Vérification si la valeur entrées par l'utilisateur est correcte
                        $data['typeRError'] = "La valeur entrée doit être 'Vente', 'Location' ou 'Autre'.";   
                    }
            
                    //Validate typeB
                    if (empty($data['typeb'])) {
                        $data['typeb'] = $bienModif['typeb'];
                    } elseif (!preg_match($textValidation, $data['typeb'])) {
                        $data['typeBError'] = 'Erreur sur le type de bien, veuillez revoir la syntaxe.';
                    } else if (($data['typeb'] !== "Maison") && ($data['typeb'] !== "Appartement") && ($data['typeb'] !== "Bureau") && ($data['typeb'] !== "Terrain") && ($data['typeb'] !== "Garage") && ($data['typeb'] !== "Autre")){ //Vérification si la valeur entrées par l'utilisateur est correcte
                        $data['typeBError'] = "La valeur entrée doit être 'Maison', 'Appartement' ou 'Bureau' ou 'Terrain' ou 'Garage' ou 'Autre'.";   
                    }
            
                    //Validate prix
                    if (empty($data['prix'])) {
                        $data['prix'] = $bienModif['prix'];
                    } elseif (!preg_match($digitValidation, $data['prix'])) {
                        $data['prixError'] = 'Erreur sur le prix, veuillez revoir la syntaxe.';
                    }
                    
            
                   // Validate localisation
                    if(empty($data['localisation'])){
                      $data['localisation'] = $bienModif['localisation'];
                    } elseif (!preg_match($textValidation, $data['localisation'])) {
                      $data['localisationError'] = 'Erreur sur la localisation, veuillez revoir la syntaxe.';
                    }
            
                    //Validate description
                    if(empty($data['description'])){
                        $data['description'] = $bienModif['description'];
                      } elseif (!preg_match($textValidation, $data['description'])) {
                        $data['descriptionError'] = 'Erreur sur la description, veuillez revoir la syntaxe.';
                      }
            
                    //S'il n'y a pas d'erreur de formulaire
                    if (empty($data['typeRError']) && empty($data['typeBError']) && empty($data['prixError']) && empty($data['localisationError']) && empty($data['descriptionError'])) {
    
            
                        //Modifier le bien depuis le model Bien
                        if ($b->modifierBien($_GET['idb'], $data['typer'],$data['typeb'], $data['prix'], $data['localisation'], $data['description'] )) {
                            //Redirection à la page du bien modifié
                            header('location: index.php?action=bien&idb='.$_GET['idb']);
                        } else {
                            die('Une erreur est survenue.');
                        }
                    }
                }
    
    
                require('View/modifyForm/modifierBien.php');
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
    
    function supprimerBien(){
        //header("location: index.php?action=utilisateurs");
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $b = new Bien();
            if($u->isAdmin($_SESSION['login'])){
                $b->supprimerBien($_GET['idb']);
                header("location: index.php?action=catalogue");
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
}


?>