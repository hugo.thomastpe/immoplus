<?php

class ControllerUtilisateur{
    function utilisateurs(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            if($u->isAdmin($_SESSION['login'])){
                $utilisateurs = $u->getUtilis();
                require("View/utilisateurs.php");
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
    
    function supprimerUtilisateur()
    {
        if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
            $u = new Utilisateur(); //On crée un utilisateur
            $daoU = new DAOUtilisateur();
            if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
                $utili = $u->getInfoUtili($_GET['idu']);
                $unUtili = $utili->fetch();
                $u->create($unUtili['idu'], $unUtili['mdp'], $unUtili['nom'], $unUtili['prenom']);
                $daoU->create($u);
                if($daoU->deleteUtili()){
                    header("location: index.php?action=utilisateurs"); //On redirige
                }
                //$u->deleteUtili($_GET['idu']); //On supprime l'utilisateur sélectionné
                
            } else{ //Sinon on affiche la page d'erreur
                require('View/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
}




?>