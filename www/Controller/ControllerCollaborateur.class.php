<?php

class ControllerCollaborateur{
    function collaborateur(){
        $c = new Collaborateur();
        $daoC = new DAOCollaborateur();
        $collab = $c->getCollab();
        $collabNom = $c->getCollabAlpha();
        $collabPoste = $c->getCollabPost();
        require("View/collaborateur.php");
    }
    
    function ajouterCollab(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $c = new Collaborateur();
            $daoC = new DAOCollaborateur();
            if($u->isAdmin($_SESSION['login'])){
    
                //Pour le formulaire
                $data = [
                    'idC' => '',
                    'nom' => '',
                    'poste' => '',
                    'photo' => '',
                    'idCError' => '',
                    'nomError' => '',
                    'posteError' => '',
                    'photoError' => ''
                ];
            
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    // Process form
                    // Sanitize POST data
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
                      $data = [
                        'idC' => trim($_POST['idC']),
                        'nom' => trim($_POST['nom']),
                        'poste' => trim($_POST['poste']),
                        'photo' => trim($_POST['photo']),
                        'idCError' => '',
                        'nomError' => '',
                        'posteError' => '',
                        'photoError' => ''
                    ];
            
                    $textValidation = "/^[a-zA-Z0-9\é\è\à\ç\.\:\!\?\%\/\ \,\'\(\)\-\_]*$/";
                    $digitValidation = "/^[0-9\.]*$/";
            
                    //Validate idC
                    if (empty($data['idC'])) { //Vérification si la zone de texte n'est pas vide
                        $data['idC'] = "Entrez une valeur pour l'id du collaborateur";
                    } elseif (!preg_match($textValidation, $data['idC'])) { //Vérification si les caractères entrés par l'utilisateur sont corrects
                        $data['idCError'] = "Erreur sur l'id du collaborateur', veuillez revoir la syntaxe.";
                    }
            
                    //Validate nom
                    if (empty($data['nom'])) {
                        $data['nom'] = 'Entrez une valeur pour le nom.';
                    } elseif (!preg_match($textValidation, $data['nom'])) {
                        $data['nomError'] = 'Erreur sur le nom, veuillez revoir la syntaxe.';
                    }
            
                    //Validate poste
                    if (empty($data['poste'])) {
                        $data['poste'] = 'Entrez une valeur pour le poste.';
                    } elseif (!preg_match($textValidation, $data['poste'])) {
                        $data['posteError'] = 'Erreur sur le poste, veuillez revoir la syntaxe.';
                    }
                    
            
                   // Validate photo
                    if(empty($data['photo'])){
                      $data['photo'] = 'Entrez une valeur pour la photo.';
                    } elseif (!preg_match($textValidation, $data['photo'])) {
                      $data['photoError'] = 'Erreur sur la photo, veuillez revoir la syntaxe.';
                    }
            
    
                    //S'il n'y a pas d'erreur de formulaire
                    if (empty($data['idCError']) && empty($data['nomError']) && empty($data['posteError']) && empty($data['photoError'])) {
    
    
                        $c->create($data['idC'], $data['nom'], $data['poste'], $data['photo']);
                        $daoC->create($c);
    
                        //Ajoute un collaborateur et vérifie si la requête s'est bien exécutée
                        if ($daoC->addCollab()) {
                            //Redirection à la page du bien modifié
                            header('location: index.php?action=collaborateur');
                        } else {
                            die('Une erreur est survenue.');
                        }
                    }
                }
    
    
                require('View/addForm/ajouterCollab.php');
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
    
    function supprimerCollab(){
        if(isset($_SESSION['login'])){
            $u = new Utilisateur();
            $c = new Collaborateur();
            $daoC = new DAOCollaborateur();
            if($u->isAdmin($_SESSION['login'])){
                $collab = $c->getInfoCollab($_GET['idc']);
                $leCollab = $collab->fetch();
                $c->create($leCollab['idc'], $leCollab['nom'], $leCollab['poste'], $leCollab['photo']);
                $daoC->create($c);
                if($daoC->deleteCollab()){
                    header("location: index.php?action=collaborateur");
                }   
            } else{
                require('View/error/error.php');
            }
        }else{
            require('View/error/error.php');
        }
    }
}


?>