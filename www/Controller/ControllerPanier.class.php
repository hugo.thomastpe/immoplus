<?php

class ControllerPanier{
    function panier()
    {
    
    if(isset($_SESSION['login'])) : //Si il y à des données de session
        $p=new Panier(); //On crée un objet panier
        $biens = $p->getPanierUtili($_SESSION['login']); //On crée une variable qui contiendra les biens dans le panier de l'utilisateur
        require('View/panier.php');
    else :
        require('View/error/error.php');
    endif;

    }

    function ajouterAuPanier()
    {
        $p = new Panier();
        $daoP = new DAOPanier();
        $p->create($_SESSION['login'], $_GET['idb']);
        $daoP->create($p);
        $daoP->addPanier();
        header("location: index.php?action=panier");
    }

    function supprimerDuPanier()
    {
        $p = new Panier();
        $daoP = new DAOPanier();
        $p->create($_SESSION['login'], $_GET['idb']);
        $daoP->create($p);
        $daoP->deletePanier();
        header("location: index.php?action=panier");

    }
}


?>