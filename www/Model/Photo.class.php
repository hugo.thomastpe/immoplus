<?php
    
    require_once('Controller/controllerBDD.php');
    
    class Photo{
        private $_idb;
        private $_idp;
        private $_nom;
        private $_lien;


        //constructeur vide puis on utilise la fonction create
        public function __construct(){

        }


        public function create( $idb, $idp, $nom, $lien){
            $this->_idb = $idb;
            $this->_idp = $idp;
            $this->_nom = $nom;
            $this->_lien = $lien;
        }

        public function getIdBien(){
            return $this->_idb; 
        }

        public function setIdBien($id) {
            $this->_idb = $id;
       }

       public function getIdPhoto(){
        return $this->_idp; 
        }

        public function setIdPhoto($id) {
            $this->_idp = $id;
        }

        public function getNom(){
            return $this->_nom; 
        }

        public function setNom($nom) {
            $this->_nom = $nom;
       }

        public function getLien(){
            return $this->_lien;
        }

        public function setLien($photo) {
            $this->_lien = $photo;
        }

        //Retourne Vrai si l'id de la photo est disponible.
        public function findPhotoById($id){
            //On récupère la connexion à la base de données
            global $db;
            $sql = "SELECT * FROM _photo WHERE idphoto=?";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($id));

            $exist = $stmt->fetchAll();

            //Si ce que retourne la requête n'est pas vide
            if($exist){
                return false; //Retourne faux car on cherche à savoir si l'id de la photo est dispo
            }

            return true;

        }

        //Récupère les infos d'une photo
        function getInfoPhoto($idb, $idp){
            //On récupère la connexion à la base de données
            global $db;
            $photo = $db->prepare('SELECT * FROM _photo WHERE idb=? and idphoto=?;');
            $photo->execute(array($idb, $idp));

            return $photo;
        }

        //récupère la première photo liée au bien
        function getPhotoBien($idb){
            //SELECT * FROM immo._photo WHERE idb=1 ORDER BY idphoto LIMIT 0,1
            //On récupère la connexion à la base de données
            global $db;
            $photo = $db->prepare('SELECT * FROM _photo WHERE idb=? ORDER BY idphoto LIMIT 0,1;');
            $photo->execute(array($idb));

            return $photo;
        }

        //récupère toute les photos du bien
        function getPhotosBien($idb){
            //SELECT * FROM immo._photo WHERE idb=1 ORDER BY idphoto
            //On récupère la connexion à la base de données
            global $db;
            $photos = $db->prepare('SELECT * FROM _photo WHERE idb=? ORDER BY idphoto;');
            $photos->execute(array($idb));

            return $photos;
        }

    }
?>