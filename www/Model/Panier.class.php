<?php

    require_once('Controller/controllerBDD.php');
    
    class Panier{


        private $_idU;
        private $_idB;


        //constructeur vide puis on utilise la fonction create
        public function __construct(){

        }


        public function create( $idU, $idB){
            $this->_idU = $idU;
            $this->_idB = $idB;
        }


        public function getIdUtili(){
            return $this->_idU; 
        }

        public function setIdUtili($id) {
            $this->_idU = $id;
       }

        public function getIdBien(){
            return $this->_idB; 
        }

        public function setIdBien($idB) {
            $this->_idB = $idB;
       }

        //Affiche les biens dans le panier de l'utilisateur
        public function getPanierUtili($login){
            //On récupère la connexion à la base de données
            global $db;

            $biens = $db->prepare('SELECT * FROM _bien INNER JOIN _panier ON _bien.idb = _panier.idb WHERE idu=? ORDER BY _bien.idb;');
            $biens->execute(array($login));

            return $biens;
        }

        //Vérifie si un bien est dans le panier d'un utilisateur
        public function estDansPanier($login, $bien){
            //On récupère la connexion à la base de données
            global $db;

            $sql = "SELECT * FROM _panier WHERE idu=? AND idb=?;";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($login, $bien));

            $exist = $stmt->fetchAll();

            //Si la requête retourne queslque chose
            if($exist){
                //Alors le bien est dans le panier de 
                return true;
            }

            return false;
        }

        //Retourne vrai si le panier d'un utilisateur est vide.
        public function isEmpty($login){
            //On récupère la connexion à la base de données
            global $db;
            $sql = "SELECT * FROM _panier WHERE idu=?;";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($login));

            $exist = $stmt->fetchAll();

            //Si ce que retourne la requête n'est pas vide
            if($exist){
                //Retourne faux
                return false;
            }
            //Sinon le panier est vide
            return true;
        }

    }

?>