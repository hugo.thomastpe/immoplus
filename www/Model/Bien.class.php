<?php
    
    require_once('Controller/controllerBDD.php');
    
    class Bien{

        private $_idB;
        private $_typeR;
        private $_typeB;
        private $_prix;
        private $_localisation;
        private $_description;

        //constructeur vide puis on utilise la fonction create
        public function __construct(){

        }


        public function create($typeR, $typeB, $prix, $localisation, $description){
            $this->_idB = null;
            $this->_typeR = $typeR;
            $this->_typeB = $typeB;
            $this->_prix = $prix;
            $this->_localisation = $localisation;
            $this->_description = $description;
        }


        public function getIdBien(){
            return $this->_idB; 
        }

        public function setIdBien($id) {
            $this->_idB = $id;
        }

        public function getTypeR(){
            return $this->_typeR; 
        }

        public function setTypeR($typer) {
            $this->_typeR = $typer;
        }

        public function getTypeB(){
            return $this->_typeB;
        }

        public function setTypeB($typeb) {
            $this->_typeB = $typeb;
        }

        public function getPrix(){
            return $this->_prix;
        }

        public function setPrix($prix) {
            $this->_prix = $prix;
        }

        public function getLocalisation(){
            return $this->_localisation;
        }

        public function setLocalisation($local) {
            $this->_localisation = $local;
        }

        public function getDescription(){
            return $this->_description;
        }

        public function setDescription($descrip) {
            $this->_description = $descrip;
        }


        //Supprime un Bien
        public function supprimerBien($idb){
            //On récupère la connexion à la base de données
            global $db;                          
            //On supprime les photos attachées au bien
            $sql = "DELETE FROM _photo WHERE idb=?;";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($idb));
            
            $stmt->closeCursor(); 
            $stmt=null;
            //On supprime le bien
            $sql2 = "DELETE FROM _bien WHERE idb=?;";
            $stmt2 = $db->prepare($sql2);
            $stmt2->execute(array($idb));
            
            $stmt2->closeCursor(); 
            $stmt2=null;
            //On supprime le bien du panier des utilisateurs
            $sql3 = "DELETE FROM _panier WHERE idb=?;";
            $stmt3 = $db->prepare($sql3);
            $stmt3->execute(array($idb));
            
            $stmt3->closeCursor(); 
            $stmt3=null;

        }

        //Modifie un bien
        public function modifierBien($idb, $typeR, $typeB, $prix, $localisation, $description){
            //UPDATE table SET colonne_1 = 'valeur 1', colonne_2 = 'valeur 2', colonne_3 = 'valeur 3' WHERE condition
            global $db;

            $sql = "UPDATE _bien SET typer=?, typeb=?, prix=?, localisation=?, description=? WHERE idb=?;";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($typeR, $typeB, $prix, $localisation, $description, $idb));
            
            $stmt->closeCursor(); 
            $stmt=null;

            return true;
        }

        //Retourne l'ensemble des biens présent dans la base de données
        function getBiens()
        {
            //On récupère la connexion à la base de données
            global $db;
            //$req = $db->query('SELECT * FROM immo._bien');
            $req = $db->prepare('SELECT * FROM _bien;');
            $req->execute();

            return $req;
        }

        //Retourne le nombre de bien présent dans la base de données
        function getNbBiens()
        {
            //On récupère la connexion à la base de données
            global $db;
            //$req = $db->query('SELECT * FROM immo._bien');
            $req = $db->prepare('SELECT COUNT(idb) FROM _bien;');
            $req->execute();
            $nb = $req->fetch();

            return $nb[0];
        }

        //Retourne les infos d'un bien
        function getBien($idb)
        {
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare('SELECT * FROM _bien WHERE idb = ?');
            $req->execute(array($idb));
            $bien = $req->fetch();

            return $bien;
        }

        //Retourne l'ensemble des biens ayant un type de recherche particulier
        function getBienTypeR($typeR)
        {
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare('SELECT * FROM _bien WHERE typer = ?');
            $req->execute(array($typeR));
            $biens = $req->fetch();

            return $biens;
        }

        //Retourne l'ensemble des biens ayant un type de bien particulier
        function getBienTypeB($typeB)
        {
            //On récupère la connexion à la base de données
            global $db;
            $biens = $db->prepare('SELECT * FROM _bien WHERE typeb = ?');
            $biens->execute(array($typeB));

            return $biens;
        }

        //Retourne le dernier bien entré dans la base de données
        function getBienRecent(){
            //SELECT * FROM immo._bien ORDER BY idb DESC LIMIT 0,1
            //On récupère la connexion à la base de données
            global $db;
            //$req = $db->query('SELECT * FROM immo._bien ORDER BY idb DESC LIMIT 0,1');
            $req = $db->prepare('SELECT * FROM _bien ORDER BY idb DESC LIMIT 0,1;');
            $req->execute();

            return $req;
        }

        //Retourne le bien le plus populaire, celui présent dans le plus de panier d'utilisateurs.
        function getBienPop(){
            //On récupère la connexion à la base de données
            global $db;

            $req = $db->prepare('SELECT * FROM _bien WHERE idb = (
                SELECT idb FROM _panier GROUP BY idb ORDER BY COUNT(idb) DESC LIMIT 0,1
                );');
            $req->execute();

            return $req;
        }

        //Retourne l'ensemble des biens correspondants aux critère de recherche.
        function rechercherBien($typeR, $typeB, $local, $prixMin, $prixMax){
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare("SELECT * FROM _bien WHERE typer=? and typeb=? and prix>=? and prix<=? and localisation LIKE %?% ORDER BY typer;");
            $req->execute(array($typeR, $typeB, $prixMin, $prixMax, $local));
            //$biens = $req->fetch();
        
            return $req;
            
        }

        public function __toString() {
            return $this->_idB.",".$this->_typeR.",".$this->_typeB.",".$this->_prix.",".$this->_localisation.",".$this->_description;
        }
    }
?>