<?php
    require_once('Controller/controllerBDD.php');
    
    class Partenaire{

        private $_idP;
        private $_nom;
        private $_logo;
        private $_lien;


        //constructeur vide puis on utilise la fonction create
        public function __construct(){

        }


        public function create( $idP, $nom, $logo, $lien){
            $this->_idP = $idP;
            $this->_nom = $nom;
            $this->_logo = $logo;
            $this->_lien = $lien;
        }


        public function getIdPartenaire(){
            return $this->_idP; 
        }

        public function setIdPartenaire($id) {
            $this->_idP = $id;
       }

        public function getNom(){
            return $this->_nom; 
        }

        public function setNom($nom) {
            $this->_nom = $nom;
       }

        public function getLogo(){
            return $this->_logo;
        }

        public function setLogo($logo) {
            $this->_logo = $logo;
       }

        public function getLien(){
            return $this->_lien;
        }

        public function setLien($lien) {
            $this->_lien = $lien;
       }


        //Retourne les partenaires
        public function getPartenaire(){
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare('SELECT * FROM _partenaire');
            $req->execute();

            return $req;
        }

        //Retourne les partenaires triés par ordre alphabétique sur leur nom
        public function getPartAlpha(){
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare('SELECT * FROM _partenaire ORDER BY _partenaire.nom');
            $req->execute();

            return $req;
        }

        function getInfoPart($idp){
            //SELECT * FROM immo._photo WHERE idb=1 ORDER BY idphoto LIMIT 0,1
            global $db;
            $photo = $db->prepare('SELECT * FROM _partenaire WHERE idp=?;');
            $photo->execute(array($idp));

            return $photo;
        }

        //Retourne le nombre de partenaire
        function getNbPart()
        {
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare('SELECT COUNT(idp) FROM _partenaire;');
            $req->execute();
            $nb = $req->fetch();

            return $nb[0];
        }

        
    }
?>