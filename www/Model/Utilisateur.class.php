<?php

require_once('Controller/controllerBDD.php');
    
    class Utilisateur{

        private $_login; 
        private $_mdp;
        private $_nom;
        private $_prenom;
        private $_admin;

        public function __construct(){          //constructeur vide puis on utilise la fonction create

        }

        public function create($login, $mdp, $nom, $prenom){
            $this->_login = $login;
            $this->_mdp = $mdp;
            $this->_nom = $nom;
            $this->_prenom = $prenom;
            $this->_admin ="non";
        }

        public function getLogin(){
            return $this->_login;
        }
    
        public function getMdp() {
            return $this->_mdp;
        }
    
        public function getNom() {
            return $this->_nom;
        }
        
        public function getPrenom() {
            return $this->_prenom;
        }

        public function getAdmin() {
            return $this->_admin;
        }

        public function setLogin(string $login) {
            $this->_login = $login;
        }
    
        public function setMdp(string $mdp) {
            $this->_mdp = $mdp;
        }
    
        public function setNom(string $nom){
            $this->_nom = $nom;
        }

        public function setPrenom(string $nom){
            $this->_nom = $nom;
        }

        //Retourne l'ensemble des utilisateurs
        function getUtilis()
        {
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->query('SELECT * FROM _utilisateur;');

            return $req;
        }

        //Ajoute un utilisateur
        public function insert() {
            //On récupère la connexion à la base de données
            global $db;                       
    
            $sql = "INSERT INTO _utilisateur VALUES(?,?,?,?,'');";
            $stmt = $db->prepare($sql);
            
            $login = $this->getLogin();
            $nom = $this->getNom();
            $prenom = $this->getPrenom();
            $mdp = password_hash($this->getMdp(), PASSWORD_DEFAULT);
            $bool = $stmt->execute(array($login, $nom, $prenom, $mdp));
            

            $stmt->closeCursor(); 
            $stmt=null;

            return $bool;

        }

        
    
        /*
            test connexion
            return true si le tuple login mdp existe
        */
        public function testConnexion(){
            //On récupère la connexion à la base de données
            global $db;
    
            $varLogin = $this->getLogin();
            $varMdp = $this->getMdp();
    
            //$sql = "SELECT * FROM connexion";
            $sql = "SELECT * FROM _utilisateur WHERE idu=? AND mdp=? LIMIT 1";
            $stmt = $db->prepare($sql);
            
            $stmt->execute(array($varLogin, $varMdp));
    
            while ($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                return true;
                if ($result->login == $varLogin && $result->mdp == $varMdp){      //ne marche pas car verif juste la premiere ligne
                    return true;
                }
            }
            return false;
        }

        //Vérifie si le mot de passe entré par un utlisateur est le même que celui de la base de données
        public function passwordVerify($mdp, $user){
            //On récupère la connexion à la base de données
            global $db;
            $sql = "SELECT * FROM _utilisateur WHERE idu=? AND mdp=? LIMIT 1";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($user, $mdp));

            $exist = $stmt->fetchAll();

            if($exist){
                return true;
            }

            return false;

        }

        //Recherche si le login est déjà présent dans la base de données
        public function findUserByLogin($login){
            //On récupère la connexion à la base de données
            global $db;
            //Requête préparée
            $sql = "SELECT * FROM _utilisateur WHERE idu=?";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($login));
            //On fetch
            $exist = $stmt->fetchAll();

            //Si la requête retourne quelque chose
            if($exist){
                return false; //Retourne faux car on cherche à savoir si le login est dispo
            }
            //Sinon le login est dispo et onn retourne vrai
            return true;

        }

        //Vérifie les infos de connexion d'un utilisateur.
        public function seConnecter($login, $password) {
            //On récupère la connexion à la base de données
            global $db;

            $sql = "SELECT * FROM _utilisateur WHERE idu=?";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($login));
    
            $row = $stmt->fetchAll();

            //On récupère le mot de passe de la base de données (crypté)
            $hashedPassword = $row[0]['mdp']; //Retourne un tableau de tableau avec en première ligne du tableau le tableau du l'utilisateur connecté. On récupère la colonne 'mdp'

            //Si les deux mots de passes sont identiques
            if (password_verify($password, $hashedPassword)) {
                //echo 'mdp valide';
                return $row; //On retourne la lign de l'utilisateur
            } else { //Sinon
                //echo 'utilisateur inexistant';
                return false; //On retourne faux
            }
        }

        //Vérifie si l'utilisateur est un admin
        public function isAdmin($login){
            //On récupère la connexion à la base de données
            global $db;
            $sql = "SELECT * FROM _utilisateur WHERE idu=?";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($login));

            $exist = $stmt->fetchAll();

            //Si la ligne retournée par la requête possède, dans la colonne 'admin', l'attribut est "oui", alors l'utilisateur est un admin
            if($exist[0]['admin'] === 'oui'){
                return true; //true = 1 
                //On retourne vrai
            }
            //Sinon ce n'est pas un admin
            return false; //false = 0

        }

        //Retourne les infos d'un utilisateur
        function getInfoUtili($login){
            //On récupère la connexion à la base de données
            global $db;
            $utili = $db->prepare('SELECT * FROM _utilisateur WHERE idu=?;');
            $utili->execute(array($login));

            return $utili;
        }

        //Retourne le nombre d'utilisateur.
        function getNbUtili()
        {
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare('SELECT COUNT(idu) FROM _utilisateur;');
            $req->execute();
            $nb = $req->fetch();

            return $nb[0];
        }

    }

    /* Test pour la class Utilisateur*/
    /*
    $obj1 = new Utilisateur();
    $obj1->create('log1','tata','toto','tata');
    $obj2 = new Utilisateur();
    $obj2->create('log2', 'tata','tata','toto');
    echo $obj1->getLogin();
    echo $obj1->getPrenom();
    $obj1->insert();
    echo $obj1->getNom()."<br>";

    if($obj1->findUserByLogin("log1")){
        echo "libre";
    }
    else{
        echo "pas libre";
    }

    print_r($obj1->isAdmin('admin'));
    $var = $obj1->isAdmin('admin');
    echo '<br/>'.$var.'<br/>';

    print_r($obj1->seConnecter('log1','tata'));

$res = $obj1->testConnexion();
echo "Si le resultat est deja present doit afficher 1, sinon 0 : ". $res. "<br>";
$res = $obj2->testConnexion();
echo "Si le resultat est deja present doit afficher 1 : ". $res;
*/

?>