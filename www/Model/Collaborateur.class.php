<?php
    require_once('Controller/controllerBDD.php');
    
    class Collaborateur{

        private $_idC;
        private $_nom;
        private $_poste;
        private $_photo;


        //constructeur vide puis on utilise la fonction create
        public function __construct(){

        }


        public function create( $idC, $nom, $poste, $photo){
            $this->_idC = $idC;
            $this->_nom = $nom;
            $this->_poste = $poste;
            $this->_photo = $photo;
        }


        public function getIdCollab(){
            return $this->_idC; 
        }

        public function setIdCollab($id) {
            $this->_idC = $id;
       }

        public function getNom(){
            return $this->_nom; 
        }

        public function setNom($nom) {
            $this->_nom = $nom;
       }

        public function getPoste(){
            return $this->_poste;
        }

        public function setPoste($poste) {
            $this->_poste = $poste;
       }

        public function getPhoto(){
            return $this->_photo;
        }

        public function setPhoto($photo) {
            $this->_photo = $photo;
       }




        //Retourne les collaborateur
        public function getCollab(){
            //On récupère la connexion à la base de données
            global $db;
            $req = $db->prepare('SELECT * FROM _collab');
            $req->execute();

            return $req;
        }

        //Retourne les collaborateurs trié par ordre alphabétique sur leur nom
        public function getCollabAlpha(){
            //SELECT * FROM immo._collab ORDER BY _collab.nom
            global $db;
            $req = $db->prepare('SELECT * FROM _collab ORDER BY _collab.nom');
            $req->execute();

            return $req;
        }

        //Retourne les collaborateurs trié par ordre alphabétique sur leur poste
        public function getCollabPost(){
            global $db;
            $req = $db->prepare('SELECT * FROM _collab ORDER BY _collab.poste');
            $req->execute();

            return $req;
        }

        function getInfoCollab($idc){
            //SELECT * FROM immo._photo WHERE idb=1 ORDER BY idphoto LIMIT 0,1
            global $db;
            $photo = $db->prepare('SELECT * FROM _collab WHERE idc=?;');
            $photo->execute(array($idc));

            return $photo;
        }

        //Retourne le nombre d'utilisateur
        function getNbCollab()
        {
            global $db;
            $req = $db->prepare('SELECT COUNT(idc) FROM _collab;');
            $req->execute();
            $nb = $req->fetch();

            return $nb[0];
        }

    }
?>