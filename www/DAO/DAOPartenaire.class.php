<?php

require('Controller/controllerBDD.php');

    class DAOPartenaire{
        private $part;
        private $connexion;
  
        public function __construct() {

        }

        public function create($p){
            $this->part = $p;
            $this->connection = null;

        }

        public function connect(){
            //On récupère la connexion à la base de données
            global $db;

            try{
                $this->connexion = $db;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }  	  
        }

        //Ajoute un partnaire à la liste
        public function addPart() {
      
	        try{
                $this->connect();
                $query = " INSERT INTO _partenaire values(:id,:nom,:logo,:lien);"; 
                $data = array( 
                ':id'=>$this->part->getIdPartenaire(),
                ':nom'=> $this->part->getNom(), 
                ':logo'=>$this->part->getLogo(),
                ':lien'=>$this->part->getLien()
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }

        //Supprime un partnaire 
        public function deletePart() {
      
	        try{
                $this->connect();
                $query = "DELETE FROM _partenaire WHERE idp=:idp;"; 
                $data = array( 
                ':idp'=>$this->part->getIdPartenaire(),
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }


        
    }

?>