<?php

require('Controller/controllerBDD.php');

    class DAOUtilisateur{
        private $utili;
        private $connexion;
  
        public function __construct() {

        }

        public function create($u){
            $this->utili = $u;
            $this->connection = null;

        }

        public function connect(){
            //On récupère la connexion à la base de données
            global $db;

            try{
                $this->connexion = $db;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }  	  
        }

        //Supprime un utilisateur
        public function deleteUtili() {
      
	        try{
                $this->connect();
                //On supprime l'utilisateur
                $query = "DELETE FROM _utilisateur WHERE idu=:idu;"; 
                $data = array( 
                ':idu'=>$this->utili->getLogin()
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                
                if($res){ //Si la suppresion s'est bien passée
                    //On supprime le panier de l'utilisateur supprimé
                    $query2 = "DELETE FROM _panier WHERE idu=:idu;"; 
                    $data2 = array( 
                    ':idu'=>$this->utili->getLogin()
                    );
                    $sth2 = $this->connexion->prepare( $query2 );
                    $res2=$sth2->execute( $data2 );

                    $this->connexion = null;
                    return $res2;
                }
                return $res;
                
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }

        
    }

?>