<?php

require('Controller/controllerBDD.php');

    class DAOPhoto{
        private $photo;
        private $connexion;
  
        public function __construct() {

        }

        public function create( $p){
            $this->photo = $p;
            $this->connection = null;

        }

        public function connect(){
            //On récupère la connexion à la base de données
            global $db;

            try{
                $this->connexion = $db;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }  	  
        }

        //Ajoute une photo à un bien
        public function addPhoto() {
      
	        try{
                $this->connect();
                $query = " INSERT INTO _photo values(:idb,:idp,:nom,:lien);"; 
                $data = array( 
                ':idb'=>$this->photo->getIdBien(),
                ':idp'=> $this->photo->getIdPhoto(), 
                ':nom'=>$this->photo->getNom(),
                ':lien'=>$this->photo->getLien()
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }

        //Supprime une photo à un bien
        public function deletePhoto() {
      
	        try{
                $this->connect();
                $query = "DELETE FROM _photo WHERE idb=:idb AND idphoto=:idp;"; 
                $data = array( 
                ':idb'=>$this->photo->getIdBien(),
                ':idp'=> $this->photo->getIdPhoto(), 
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }


        
    }

?>