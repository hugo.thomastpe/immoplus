<?php

require('Controller/controllerBDD.php');

    class DAOPanier{
        private $panier;
        private $connexion;
  
        public function __construct() {

        }

        public function create($p){
            $this->panier = $p;
            $this->connection = null;

        }

        public function connect(){
            global $db;

            try{
                $this->connexion = $db;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }  	  
        }

        //Ajoute un bien dans le panier d'un utilisateur
        public function ajouterPanier($login, $bien){
            //On récupère la connexion à la base de données
            global $db;                       
    
            $sql = "INSERT INTO _panier VALUES(?,?);";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($login, $bien));
            
            $stmt->closeCursor(); 
            $stmt=null;
        }

        //Ajoute un bien dans le panier d'un utilisateur
        public function addPanier() {
      
	        try{
                $this->connect();
                $query = " INSERT INTO _panier values(:idu,:idb);"; 
                $data = array( 
                ':idu'=>$this->panier->getIdUtili(),
                ':idb'=> $this->panier->getIdBien() 
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }

        //Supprime un bien du panier d'un utilisateur
        public function deletePanier() {
      
	        try{
                $this->connect();
                $query = "DELETE FROM _panier WHERE idu=:idu AND idb=:idb;"; 
                $data = array( 
                    ':idu'=>$this->panier->getIdUtili(),
                    ':idb'=> $this->panier->getIdBien() 
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }

    }

?>