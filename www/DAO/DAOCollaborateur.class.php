<?php

require('Controller/controllerBDD.php');

    class DAOCollaborateur{
        private $collab;
        private $connexion;
  
        public function __construct() {

        }

        public function create( $c){
            $this->collab = $c;
            $this->connection = null;

        }

        public function connect(){
            //On récupère la connexion à la base de données
            global $db;

            try{
                $this->connexion = $db;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }  	  
        }

        //Ajoute un collaborateur à la liste
        public function addCollab() {
      
	        try{
                $this->connect();
                $query = " INSERT INTO _collab values(:id,:nom,:poste,:photo);"; 
                $data = array( 
                ':id'=>$this->collab->getIdCollab(),
                ':nom'=> $this->collab->getNom(), 
                ':poste'=>$this->collab->getPoste(),
                ':photo'=>$this->collab->getPhoto()
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }

        //Supprime un collaborateur 
        public function deleteCollab() {
      
	        try{
                $this->connect();
                $query = "DELETE FROM _collab WHERE idc=:idc;"; 
                $data = array( 
                ':idc'=>$this->collab->getIdCollab(),
                );
                $sth = $this->connexion->prepare( $query );
                $res=$sth->execute( $data );
                $this->connexion = null;
                return $res;
            }catch (PDOException $e){
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }


        
    }

?>