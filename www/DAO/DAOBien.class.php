<?php

require('Controller/controllerBDD.php');

class DAOBien{
  private $bien;
  private $connexion;
  
  public function __construct() {

}

public function create($b){
	$this->bien = $b;
	$this->connection = null;

}
  
  public function connect(){
	  //On récupère la connexion à la base de données
		global $db;

		try{
			$this->connexion = $db;
		}catch (PDOException $e){
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}  	  
	}

  	public function getBien() {
     return $this->bien;
  	}
  
  	public function setBien($b) {
       $this->bien = $b;
  	}
  
  public function addBien() {
      
	try{
		$this->connect();
		$query = " INSERT INTO _bien values(NULL, :tpr,:tpb,:prix,:loc,:descr)"; 
		$data = array( 
		':tpr'=>$this->bien->getTypeR(),
		':tpb'=> $this->bien->getTypeB(), 
		':prix'=>$this->bien->getPrix(),
		':loc'=>$this->bien->getLocalisation(),
		':descr'=>$this->bien->getDescription()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
  public function delete() {
      
	try{
		$this->connect();
		$query = " delete from _bien where idb=:id "; 
		$data = array( 
		':id'=>$this->bien->getIdBien()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
  public function update() {
      
	try{
		$this->connect();
		$query = " update Adresse set typer=:tpr, typeb=:tpb, prix=:prix, localisation=:loc, description=:descr where idb=:id "; 
		$data = array( 
			':id'=>$this->bien->getIdBien(),
			':tpr'=>$this->bien->getTypeR(),
			':tpb'=> $this->bien->getTypeB(), 
			':prix'=>$this->bien->getPrix(),
			':loc'=>$this->bien->getLocalisation(),
			':descr'=>$this->bien->getDescription()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
   
}


?>