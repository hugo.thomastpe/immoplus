<?php $title = 'Panier'; ?>

<?php ob_start(); ?>

    <div class="container white catalogue z-depth-2 col-10 top">

        <h1 class="display-3">Panier de <?php echo $_SESSION['login']; ?> </h1>
 
        <div class="catalogue-liste">

            <?php 
                $p=new Panier();
                if ($p->isEmpty($_SESSION['login'])){ //Si le panier est vide :
            ?>
                <!--Message Panier Vide-->
                <div class="container">
    	            <div class="jumbotron">
			            <h2 class="display-4">Panier vide</h2> 
			            <p class="lead">Votre panier est vide, prenez le temps de visiter notre catalogue !</p>
			            <a class="btn btn-primary" href="index.php?action=catalogue" role="button">Notre catalogue</a>
		            </div>
                </div>
                
            <?php
                } else { //Sinon
                    
                    while ($data = $biens->fetch()) //Pour chaque bien
                    {
                        $p=new Photo();
                        $photo=$p->getPhotoBien($data['idb']); //On prépare la requête pour récupérer la première photo du bien
                        $pic=$photo->fetch(); //On lance la requête et récupère le résultat dans la variable pic
                        //print_r($pic);

            ?>
            
            <div class="bien container col-3 row bien-catalogue">
                
                <img src="<?= $pic['lien'] ?>" alt="<?= $pic['nom'] ?>" width="250" height="200"> <!--On affiche la première image-->

                <article class="contenu col-12">
                    <h3>
                        <?= htmlspecialchars($data['typeb']) ?> - <?= htmlspecialchars($data['typer']) ?><!--On affiche la type de bien et le type de recherche-->
                    </h3>
                
                    <p> <!--On affiche la descrition et un lien pour accéder à la page complète du bien-->
                        <?= 
                            //nl2br : prend en compte les retour à la ligne et les traduit en balise html
                            //htmlspecialchars : prend en compte les caractères spéciaux et les traduis en entités html
                            nl2br(htmlspecialchars(substr($data['localisation'], 0, 50))) 
                        ?>
                        ...<br />
                        <em><a class="btn btn-primary" width=100% href="index.php?action=bien&amp;idb=<?= $data['idb'] ?>">Voir le bien</a></em>
                    </p>

                    <a class="btn btn-danger" width=100% href="index.php?action=supprimerPanier&amp;idb=<?= $data['idb'] ?>">Supprimer du panier</a>
                </article>
            </div>

            <?php
                    } //end While
                    $biens->closeCursor();
                } //end If
            ?>

        </div>
    </div>

<?php $content = ob_get_clean();?> 

<!--Tout le code écrit entre ob_start() (ligne 3) et ob_get_clean() est inclus dans la variable content. -->
<!-- Cela permet d'intégrer tout cela au fichier common/template.php et d'afficher la page comme il faut. -->

<!-- une autre solution aurait été de créer deux vues header et footer mais cette solution est moins flexible.-->

<?php require('template.php'); ?>