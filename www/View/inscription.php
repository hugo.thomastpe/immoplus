<?php $title = 'Inscription'; ?>

<?php ob_start(); ?>

<div class="container-login accueil container ">
    <div class="wrapper-login inscription">
        
    </div>
    <div>
    <h2 class="display-2">Inscription</h2>
        <p class="lead">Complétez les informations suivantes afin de créer un compte :</p>
        <hr>
        <form
            id="register-form"
            method="POST"
            action="index.php?action=inscription"
        >
            <p>Login : <input type="text" placeholder="Login *" name="login"></p>
            <span class="invalidFeedback">
                <?php echo $data['loginError']; ?>
            </span>

            <p>Nom : <input type="text" placeholder="Nom *" name="nom"></p>
            <span class="invalidFeedback">
                <?php echo $data['nomError']; ?>
            </span>

			<p>Prénom : <input type="text" placeholder="Prénom *" name="prenom"></p>
            <span class="invalidFeedback">
                <?php echo $data['prenomError']; ?>
            </span>

            <p><em>Le mot de passe doit contenir au minimum huit caractères et un chiffre.</em></p>

            <p>Mot de passe : <input type="password" placeholder="Mot de passe *" name="password"></p>
            <span class="invalidFeedback">
                <?php echo $data['passwordError']; ?>
            </span>

            <p>Confirmer le mot de passe : <input type="password" placeholder="Confirmer *" name="confirmPassword"></p>
            <span class="invalidFeedback">
                <?php echo $data['confirmPasswordError']; ?>
            </span>

            <button id="submit" type="submit" value="submit" class="btn btn-primary">S'inscrire</button>

            <p class="options">Déjà inscrit ? <a href="index.php?action=connexion">Connectez-vous !</a></p>
        </form>
    </div>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>