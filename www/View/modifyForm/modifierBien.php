<?php $title = 'Modifier un bien'; ?>

<?php ob_start(); ?>

<div class="container white accueil z-depth-1 col-10">
    <h1 class="display-1">Modification du bien <?= $_GET['idb'] ?></h1>

    <div class="jumbotron">

    <p class="lead">Si une entrée est vide, aucune modification ne sera apportée au champ en question.</p>

    <form
            id="register-form"
            method="POST"
            action="index.php?action=modifierBien&amp;idb=<?= $_GET['idb'] ?>"
        >
            <p>Type de recherche : <input type="text" value="<?= $bienModif['typer']; ?>" name="typer"></p>
            <span class="invalidFeedback">
                <?php echo $data['typeRError']; ?>
            </span>

            <p>Type de bien : <input type="text" value="<?= $bienModif['typeb']; ?>" name="typeb"></p>
            <span class="invalidFeedback">
                <?php echo $data['typeBError']; ?>
            </span>

			<p>Prix : <input type="text" value="<?= $bienModif['prix']; ?>" name="prix"></p>
            <span class="invalidFeedback">
                <?php echo $data['prixError']; ?>
            </span>

            <p>Localisation : <input type="text" value="<?= $bienModif['localisation']; ?>" name="localisation"></p>
            <span class="invalidFeedback">
                <?php echo $data['localisationError']; ?>
            </span>

            <p>Description : <input type="text" value="<?= $bienModif['description']; ?>" name="description" size="90"></p>
            <span class="invalidFeedback">
                <?php echo $data['descriptionError']; ?>
            </span>

            <div class="row">
                <button id="submit" type="submit" value="submit" class="btn btn-primary">Enregistrer les modifications</button>   
                <a class="btn btn-danger" href="index.php?action=bien&amp;idb=<?= $_GET['idb'] ?>">Annuler les modifications</a>
            </div>
        </form>

        
    </div>

</div>

<?php $content = ob_get_clean();?> 

<?php require('View/template.php'); ?>