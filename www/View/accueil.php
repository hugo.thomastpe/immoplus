<?php $title = 'Accueil - ImmoPlus'; ?>

<?php ob_start(); ?>



<div class="accueil container col-10">
  <div class="jumbotron">
    <h1 class="display-3">Accueil</h1>
    <?php if(isset($_SESSION['login'])) : ?>
      <h3 class="display-5">Bienvenue sur le site <?php echo $_SESSION['login'] ?> !</h3>
    <?php else : ?>
      <h3 class="display-5">Bienvenue sur le site !</h3>
    <?php endif; ?>
  </div>
  <div class="jeu-accueil d-flex">
    <div class="populaire col-6">
      <h2>Le bien le plus populaire :</h2>
        <div class="catalogue-liste col-12">
		      <?php $bien = $bienpop->fetch();
                $p=new Photo();
                $photo=$p->getPhotoBien($bien['idb']); 
                $pic=$photo->fetch(); 
                    ?>

			      <div class="bien">
				      
                    <img src="<?php echo $pic['lien'] ?>" width="250" height="200">
				      
                    <article class="contenu">
					      
                          <p><?= htmlspecialchars($bien['typeb']) ?> - <?= htmlspecialchars($bien['typer']) ?></p>

                          <p> <!--On affiche la descrition et un lien pour accéder à la page complète du bien-->
                              <?= 
                                  //nl2br : prend en compte les retour à la ligne et les traduit en balise html
                                  //htmlspecialchars : prend en compte les caractères spéciaux et les traduis en entités html
                                  nl2br(htmlspecialchars(substr($bien['description'], 0, 50))) 
                              ?>
                              ...<br />
                              <em><a class="btn btn-primary" width=100% href="index.php?action=bien&amp;idb=<?= $bien['idb'] ?>">Voir le bien</a></em>
                          </p>
              
                      </article>

			      </div >
	      </div>
    </div>
    <div class="nouveaute col-6">
      <h2>Nouveauté :</h2>
        <div class="catalogue-liste col-12">
		      <?php $bien2 = $bienrecent->fetch();
                 $p=new Photo();
                 $photo=$p->getPhotoBien($bien2['idb']);
                  $pic=$photo->fetch(); 
          ?>
			    <div class="bien">

            <img src="<?php echo $pic['lien'] ?>" width="250" height="200">

				      <article class="contenu">
					      
                <p><?= htmlspecialchars($bien2['typeb']) ?> - <?= htmlspecialchars($bien2['typer']) ?></p>

					      <p> <!--On affiche la descrition et un lien pour accéder à la page complète du bien-->
                  <?= 
                    //nl2br : prend en compte les retour à la ligne et les traduit en balise html
                    //htmlspecialchars : prend en compte les caractères spéciaux et les traduis en entités html
                    nl2br(htmlspecialchars(substr($bien2['description'], 0, 50))) 
                  ?>
                  ...<br />
                    <em><a class="btn btn-primary" width=100% href="index.php?action=bien&amp;idb=<?= $bien2['idb'] ?>">Voir le bien</a></em>
                </p>

				      </article>
			    </div >
	      </div>
    </div>
  </div>
  <?php
		if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
	?>
    <div class="jumbotron jumbo-accueil">
      <h3 class="display-4">Accéder à votre panier</h3>
      <p class="lead">Votre panier a-t'il changé depuis votre dernière visite ? découvrez-le en cliquant sur le bouton pour accéder à cotre panier :</p>
      <a class="btn btn-primary" width=100% href="index.php?action=panier">Votre panier</a>
    </div>
  <?php
      $u = new Utilisateur(); //On crée un utilisateur
			if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
	?>
    <div class="jumbotron jumbo-accueil">
        <h3 class="display-4">Accéder à la liste des utilisateurs</h3>
        <p class="lead">Besoin de modifier ou supprimer un utilisateur ? Accédez à la liste des utilisateurs en cliquant sur ce bouton :</p>
        <a class="btn btn-primary" width=100% href="index.php?action=utilisateurs">Liste des utilisateurs</a>
    </div>
  <?php
      }
		} else {
  ?>

    <div class="jumbotron jumbo-accueil">
        <h3 class="display-4">Créez un compte !</h3>
        <p class="lead">Rejoignez-nous et ajoutez des biens à votre panier. Vous pourrez ainsi les retrouver plus rapidement. Cliquez ici pour vous inscrire :</p>
        <a class="btn btn-primary" width=100% href="index.php?action=inscription">Inscription</a>
    </div>

  <?php
    }
	?>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>