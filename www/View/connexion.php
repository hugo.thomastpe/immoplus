
<?php $title = 'Connexion'; ?>

<?php ob_start(); ?>

<div class="container-login accueil container">
    <div class="wrapper-login connexion">
        <h2 class="display-2">Connexion</h2>
        <hr>

        <form action="index.php?action=connexion" method ="POST">
            <p> Login : <input type="text" placeholder="Login *" name="login"></p>
            <span class="invalidFeedback">
                <?php echo $data['loginError']; ?>
            </span>

            <p> Mot de passe : <input type="password" placeholder="Mot de passe *" name="password"></p>
            <span class="invalidFeedback">
                <?php echo $data['passwordError']; ?>
            </span>

            <button id="submit" type="submit" value="submit" class="btn btn-primary">Submit</button>

            <p class="options">Pas encore inscrit ? <a href="index.php?action=inscription">Créer un compte !</a></p>
        </form>
    </div>
</div>


        <?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>