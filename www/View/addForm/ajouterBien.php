<?php $title = 'Ajouter un bien'; ?>

<?php ob_start(); ?>

<div class="container white accueil z-depth-1 col-10">
    <h1 class="display-1">Ajouter un bien au catalogue</h1>

    <div class="jumbotron">
        <p class="lead">Respecter les codes d'insertion permet une meilleur indexation du bien.</p>

        <form
            id="register-form"
            method="POST"
            action="index.php?action=ajouterBien"
        >
            <p> Type de recherche : <input type="text" placeholder="Vente - Location ou Autre" name="typer" required></p>
            <span class="invalidFeedback">
                <?php echo $data['typeRError']; ?>
            </span>

            <p>Type de bien : <input type="text" placeholder="Maison - Appartement - Bureau - Terrain ou Autre" name="typeb" required></p>
            <span class="invalidFeedback">
                <?php echo $data['typeBError']; ?>
            </span>

			<p>Prix : <input type="text" placeholder="ex : 150117" name="prix" required></p>
            <span class="invalidFeedback">
                <?php echo $data['prixError']; ?>
            </span>

            <p>Localisation : <input type="text" placeholder="Ville (CP)" name="localisation" required></p>
            <span class="invalidFeedback">
                <?php echo $data['localisationError']; ?>
            </span>

            <p>Description : <input type="text" placeholder="3 cuisines, 1 chambre, ..." name="description" required size="90"></p>
            <span class="invalidFeedback">
                <?php echo $data['descriptionError']; ?>
            </span>

            <div class="row">
                <button id="submit" type="submit" value="submit" class="btn btn-primary">Ajouter le bien</button>   
                <a class="btn btn-danger" href="index.php?action=catalogue">Annuler</a>
            </div>
        </form>

        
    </div>

</div>

<?php $content = ob_get_clean();?> 

<?php require('View/template.php'); ?>