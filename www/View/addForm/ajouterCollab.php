<?php $title = 'Ajouter un collaborateur'; ?>

<?php ob_start(); ?>

<div class="container white accueil z-depth-1 col-10">
    <h1 class="display-1">Ajouter un Collaborateur</h1>

    <div class="jumbotron">
        <p class="lead">Respecter les codes d'insertion permet une meilleur indexation du collaborateur.</p>

        <form
            id="register-form"
            method="POST"
            action="index.php?action=ajouterCollab"
        >
            <p>Id du collaborateur : <input type="text" placeholder="Prenom+Première Letrre du nom" name="idC" required></p>
            <span class="invalidFeedback">
                <?php echo $data['idCError']; ?>
            </span>

            <p> Nom et Prénom : <input type="text" placeholder="Nom Prénom" name="nom" required></p>
            <span class="invalidFeedback">
                <?php echo $data['nomError']; ?>
            </span>

			<p>Poste : <input type="text" placeholder="Poste" name="poste" required></p>
            <span class="invalidFeedback">
                <?php echo $data['posteError']; ?>
            </span>

            <p>Photo du collaborateur : <input type="text" placeholder="Lien de la photo" name="photo" required></p>
            <span class="invalidFeedback">
                <?php echo $data['photoError']; ?>
            </span>

            <div class="row">
                <button id="submit" type="submit" value="submit" class="btn btn-primary">Ajouter le Collaborateur</button>   
                <a class="btn btn-danger" href="index.php?action=collaborateur">Annuler</a>
            </div>
        </form>

        
    </div>

</div>

<?php $content = ob_get_clean();?> 

<?php require('View/template.php'); ?>