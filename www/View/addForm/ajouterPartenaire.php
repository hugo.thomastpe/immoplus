<?php $title = 'Ajouter un partenaire'; ?>

<?php ob_start(); ?>

<div class="container white accueil z-depth-1 col-10">
    <h1 class="display-1">Ajouter un Partenaire</h1>

    <div class="jumbotron">
        <p class="lead">Respecter les codes d'insertion permet une meilleur indexation du partenaire.</p>

        <form
            id="register-form"
            method="POST"
            action="index.php?action=ajouterPart"
        >
            <p>Id du partenaire : <input type="text" placeholder="Nom sans espace" name="idP" required></p>
            <span class="invalidFeedback">
                <?php echo $data['idPError']; ?>
            </span>

            <p>Nom : <input type="text" placeholder="Nom" name="nom" required></p>
            <span class="invalidFeedback">
                <?php echo $data['nomError']; ?>
            </span>

			<p>Logo : <input type="text" placeholder="Lien du logo" name="logo" required></p>
            <span class="invalidFeedback">
                <?php echo $data['logoError']; ?>
            </span>

            <p>Site Web : <input type="text" placeholder="Lien du site web" name="lien" required></p>
            <span class="invalidFeedback">
                <?php echo $data['lienError']; ?>
            </span>

            <div class="row">
                <button id="submit" type="submit" value="submit" class="btn btn-primary">Ajouter le Partenaire</button>   
                <a class="btn btn-danger" href="index.php?action=partenaire">Annuler</a>
            </div>
        </form>

        
    </div>

</div>

<?php $content = ob_get_clean();?> 

<?php require('View/template.php'); ?>