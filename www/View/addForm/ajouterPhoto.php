<?php $title = 'Ajouter une photo'; ?>

<?php ob_start(); ?>

<div class="container white accueil z-depth-1 col-10">
    <h1 class="display-1">Ajouter une photo au bien n° <?= $_GET['idb'] ?></h1>
    <p class="lead"><u>Info sur le bien :</u>
        <br>
        Id Bien : <?= $infoBien['idb'] ?> 
        <br>
        Type de Bien : <?= $infoBien['typeb'] ?>
        <br>
    </p>

    <div class="jumbotron">
    <form
            id="register-form"
            method="POST"
            action="index.php?action=ajouterPhoto&amp;idb=<?= $_GET['idb'] ?>"
        >
            <p>Id de la photo : <input type="text" placeholder="TypeBienN°Bien.NomPhoto *" name="idphoto"></p>
            <span class="invalidFeedback">
                <?php echo $data['idPhotoError']; ?>
            </span>

            <p>Nom : <input type="text" placeholder="Nom *" name="nom"></p>
            <span class="invalidFeedback">
                <?php echo $data['nomError']; ?>
            </span>

			<p>Photo : <input type="text" placeholder="Lien vers l'image *" name="lien"></p>
            <span class="invalidFeedback">
                <?php echo $data['lienError']; ?>
            </span>

            <div class="row">
                <button id="submit" type="submit" value="submit" class="btn btn-primary">Ajouter la photo</button>   
                <a class="btn btn-danger" href="index.php?action=bien&amp;idb=<?= $_GET['idb'] ?>">Annuler</a>
            </div>
        </form>

        
    </div>

</div>

<?php $content = ob_get_clean();?> 

<?php require('View/template.php'); ?>