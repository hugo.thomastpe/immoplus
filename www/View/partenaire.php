<?php $title = 'Partenaires'; ?>

<?php ob_start(); ?>



<script>
	//Script ajax avec les fonctions modifier l'ordre d'affichage des collaborateurs

    function basique()
	{
		var req = null;
		if (window.XMLHttpRequest)
		{
			req = new XMLHttpRequest();

		}
		else if (window.ActiveXObject)
		{
			try {
				req = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e)
			{
				try {
					req = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}


		req.onreadystatechange = function()
		{

			if(req.readyState == 4)
			{
				if(req.status == 200)
				{

					document.getElementById("part_basique").style.display = "flex";
                    document.getElementById("part_nom").style.display = "none";

				}
				else
				{
					alert(value="Error: returned status code " + req.status + " " + req.statusText);
				}
			}
		};
		req.open("GET", "index.php?action=collaborateur", true);
		req.send(null);
	}

    function alphaNom()
	{
		var req = null;
		if (window.XMLHttpRequest)
		{
			req = new XMLHttpRequest();
		}
		else if (window.ActiveXObject)
		{
			try {
				req = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e)
			{
				try {
					req = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}


		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{

                    document.getElementById("part_basique").style.display = "none";
                    document.getElementById("part_nom").style.display = "flex";

				}
				else
				{
					alert(value="Error: returned status code " + req.status + " " + req.statusText);
				}
			}
		};
		req.open("GET", "index.php?action=collaborateur", true);
		req.send(null);
	}

</script>






<div class="container catalogue profile-page">
    <h1 class="display-3">Nos Partenaires</h1>
    
    <div class="jumbotron collab_jumbo">
        <p class="lead">Veuillez choisir la méthode d'affichage :</p>
        <input type="radio" id="Choice1"
            name="contact" value="basique" onchange="basique()">
        <label for="collabChoice1">Ordre d'entrée</label>

        <input type="radio" id="Choice2"
            name="contact" value="nom" onchange="alphaNom()">
        <label for="collabChoice2">Alphabétique sur le nom</label>

    </div>

			<?php
			    if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
                    $u = new Utilisateur();
					if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
			?>
						<a class="btn btn-primary" width=100% href="index.php?action=ajouterPart">Ajouter un partenaire</a>
						<hr>
			<?php
					}
				}
            ?>

    <div class="row" id="part_basique">
            <?php
                while ($data = $partenaires->fetch()) //Pour chaque partenaire
                {
            ?>
        <div class="col-3 collaborat collab-basique" >
            <img src="<?= $data['logo'] ?>" ><!--On affiche la photo-->

            <hr>

            <div>
                <h3>
                    <?= htmlspecialchars($data['nom']) ?><!--On affiche le nom-->
                </h3>
                <a class="btn btn-primary" width=100% href="<?= $data['lien'] ?>" target="_blank">Site Web</a>

				<?php
					if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
						if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
				?>
					<hr>
					<a class="btn btn-danger" width=100% href="index.php?action=supprimerPart&amp;idp=<?= $data['idp'] ?>">Supprimer le partenaire</a>
				<?php
						}
					}
                ?>

            </div>
        </div>

            <?php } ?>
    </div>

    <div class="row" id="part_nom">
            <?php
                while ($data = $partNom->fetch()) //Pour chaque partenaire
                {
            ?>
        <div class="col-3 collaborat collab-nom" >
            <img src="<?= $data['logo'] ?>" ><!--On affiche la photo-->

            <hr>

            <div>
                <h3>
                    <?= htmlspecialchars($data['nom']) ?><!--On affiche le nom-->
                </h3>
                <a class="btn btn-primary" width=100% href="<?= $data['lien'] ?>" target="_blank">Site Web</a>

				<?php
					if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
						if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
				?>
					<hr>
					<a class="btn btn-danger" width=100% href="index.php?action=supprimerPart&amp;idp=<?= $data['idp'] ?>">Supprimer le partenaire</a>
				<?php
						}
					}
                ?>

            </div>
        </div>

            <?php } ?>
    </div>

</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>