<?php $title = 'Erreur'; ?>

<?php ob_start(); ?>

<div class="container white accueil z-depth-1 col-10">
    <h1 class="display-1">Erreur <span class="errorcode">404</span></h1>
    <p class="lead">La page que vous recherchez a peut-être été supprimée, son nom a été modifié ou elle est temporairement indisponible.</p>
    <p class="lead">Veuillez essayer de <a href="index.php">retourner à la page</a>.</p>

</div>

<?php $content = ob_get_clean();?> 

<?php require('View/template.php'); ?>