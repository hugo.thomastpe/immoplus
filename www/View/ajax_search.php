<?php

require_once("../Controller/controllerBDD.php");

function getPhotoBien($idb){
    //On récupère la connexion à la base de données
    global $db;
    $photo = $db->prepare('SELECT * FROM _photo WHERE idb=? ORDER BY idphoto LIMIT 0,1;');
    $photo->execute(array($idb));

    return $photo;
}

function rechercherBien($typeR, $typeB, $local, $prixMin, $prixMax){
    global $db;

    $req = $db->prepare("SELECT * FROM _bien WHERE typer=? and typeb=? and prix>=? and prix<=? and localisation LIKE ? ORDER BY typer asc;");
    $req->execute(array($typeR, $typeB, $prixMin, $prixMax, "%".$local."%"));
    $biens = $req;
    ?>
    <div class="catalogue-liste row">

    <?php
        while ($data = $biens->fetch()) //Pour chaque bien
        {
            $photo=getPhotoBien($data['idb']); //On prépare la requête pour récupérer la première photo du bien
            $pic=$photo->fetch(); //On lance la requête et récupère le résultat dans la variable pic

    ?>
    
    <div class="bien container col-3 row bien-catalogue">
        
        <img src="<?= $pic['lien'] ?>" alt="<?= $pic['nom'] ?>" width="225" height="200"> <!--On affiche la première image-->

        <article class="contenu col-12">
            <h3>
                <?= htmlspecialchars($data['typeb']) ?> - <?= htmlspecialchars($data['typer']) ?><!--On affiche la type de bien et le type de recherche-->
            </h3>
        
            <p> <!--On affiche la descrition et un lien pour accéder à la page complète du bien-->
                <?= 
                    //nl2br : prend en compte les retour à la ligne et les traduit en balise html
                    //htmlspecialchars : prend en compte les caractères spéciaux et les traduis en entités html
                    nl2br(htmlspecialchars(substr($data['description'], 0, 50))) 
                ?>
                ...<br />
                <em><a class="btn btn-primary" width=100% href="index.php?action=bien&amp;idb=<?= $data['idb'] ?>">Voir le bien</a></em>
            </p>
        </article>
    </div>

    <?php
        }
        $biens->closeCursor();
    ?>

</div>

<?php
    return $req;

    
}

//Ici tu récupère le cp de la localisation
$cp = preg_replace('/[^0-9]/', '', $_GET['localite']);

//ici tu exécutes la fonction
$biens = rechercherBien($_GET['typSearch'], $_GET['typBien'],$cp,$_GET['prixMin'],$_GET['prixMax']);
?>