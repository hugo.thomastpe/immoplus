<?php $title = 'Utilisateurs'; ?>

<?php ob_start(); ?>



<div class="container white liste_collectionneurs z-depth-1 col-10">
				<h1 class="col-12 display-3">Liste des utilisateurs</h1>
				<div class="row col-12">	
					<table class="col-12">
						<thead> 
							<th>IDC (Pseudo)</th>
							<th>Nom</th>
							<th>Prénom</th>
							<th>Mot de passe</th>
							<th></th>

							<tr>
						</thead>
						<tbody>
								<?php while ($data = $utilisateurs->fetch()) { //Pour chaque utlisateurs?>

                                    <script>
                                        var idu = <?php echo json_encode($data['idu']); ?>;
                                        function supprimer()
	                                    {
		                                    var req = null;
		                                    if (window.XMLHttpRequest)
		                                    {
			                                    req = new XMLHttpRequest();
		                                    }
		                                    else if (window.ActiveXObject)
		                                    {
			                                    try {
				                                    req = new ActiveXObject("Msxml2.XMLHTTP");
			                                    } catch (e)
			                                        {
				                                        try {
					                                        req = new ActiveXObject("Microsoft.XMLHTTP");
				                                        } catch (e) {}
			                                        }
		                                        }
		                                        req.onreadystatechange = function()
		                                        {
			                                        if(req.readyState == 4)
			                                        {
				                                        if(req.status == 200)
				                                        {
					                                        //var element =  document.getElementById('suppr'); //On prend l'élément 'suppr'
					                                        console.log(idu);
                    
                                                            alert("Suppression de l'utilisateur réalisée avec succès!");
				                                        }
				                                        else
				                                        {
					                                        alert(value="Error: returned status code " + req.status + " " + req.statusText);
				                                        }
			                                        }
		                                        };
		                                        req.open("GET", "index.php?action=supprimerUtilisateur&idu="+idu, true);
		                                        req.send(null);
	                                    }
                                    </script>

								    <?php if(!$u->isAdmin($data['idu'])) { //Si l'utilisateur n'est pas un admin : On affiche ses infos?>
								        <?php echo "<td>".$data['idu']."</td>"; ?>
								        <?php echo "<td>".$data['nom']."</td>"; ?>
								        <?php echo "<td>".$data['prenom']."</td>"; ?>
								        <?php echo "<td>".$data['mdp']."</td>"; ?>
								        <td><a class="btn btn-danger" href="index.php?action=supprimerUtilisateur&amp;idu=<?= $data['idu'] ?>">Supprimer</a></td>
								    <?php } //end if ?>
						</tr>
						        <?php } //end while ?>
						</tbody>
					</table>
				</div>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>