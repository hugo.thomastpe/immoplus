<?php $title = 'Contact'; ?>

<?php ob_start(); ?>

<div class="container white catalogue z-depth-2 col-10 top">

    <h1 class="display-2">Nous contacter</h1>

    <hr>

    <div class="container">
    	<div class="jumbotron">
        <h2 class="display-4">Par téléphone !</h2>
            <div class="row">
                <div class="col-6 texte-tel">
                    </br>
                    </br>
			        <p class="lead">Vous souhaitez avoir plus de rensignements sur un bien ? <br/> Ou vous souhaitez prendre rendez-vous dans notre agence ?</p>

                    <p class="lead"><a href="tel:+33711223344">Cliquez ici pour appeler le 07 11 22 33 44</a></p>
                </div>
                <div class="col-5 div_image_contact">
                    <img src="images/call.png" alt="Nous contacter par téléphone" width=250/>
                    <!--<img src="images/charte_gratuit.png" alt="Nous contacter par téléphone" width=50/>-->
                </div>
            </div>
		</div>
    </div>

    <?php
		if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
	?>
    <div class="container">
        <div class="jumbotron">
            <h2 class="display-4">Un problème avec votre compte ?</h2>
            <p class="lead">Vous souhaitez supprimer votre compte ? Envoyez nous un mail avec votre Login et votre Nom/Prénom associé au compte en question</p>
            <a class="btn btn-primary" width=100% 
                href="mailto:webmaster.immoplusfrance@hugo-thomas.com?subject=Suppression du compte <?= $_SESSION['login'] ?>
                        &body=Merci d'indiquer votre Nom et Prénom : "
            >Nous contacter par mail</a>
        </div>
    </div>
    <?php
      }
	?>

    <div class="container">
    	<div class="jumbotron">
        <h2 class="display-4">Nous rendre visite !</h2>
            <div class="row">
                
                <div class="col-7">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5328.089040586757!2d-1.676380891002833!3d48.10937689079925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x480ede3510db846d%3A0x104cf48ebf45a2a6!2s13%20Rue%20du%20Pr%C3%A9%20Bott%C3%A9%2C%2035000%20Rennes!5e0!3m2!1sfr!2sfr!4v1640540619836!5m2!1sfr!2sfr" width="500" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>

                <div class="col-3 texte-adresse">
                    </br>
                    </br>
			        <p class="lead">Envie de discuter ? <br/> Ou vous avez rendez-vous avec l'un de nos agents ?</p>

                    <p class="lead">Nous nous ferons un plaisir de vous accueillir au <strong>13 Rue du Pré Botté, 35000 Rennes</strong>!</p>
                </div>
            </div>
		</div>
    </div>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>