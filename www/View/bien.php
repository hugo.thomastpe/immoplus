<?php $title = 'Bien'; ?>

<?php ob_start(); ?>




<script>
	//Script ajax avec les fonctions ajouter et supprimer pour Ajouter/Supprimer du panier sans recharger les éléments de la page
    var idbien = <?php echo json_encode($bien['idb']); ?>;
    
    function supprimer()
	{
		var req = null;



		if (window.XMLHttpRequest)
		{
			req = new XMLHttpRequest();

		}
		else if (window.ActiveXObject)
		{
			try {
				req = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e)
			{
				try {
					req = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}


		req.onreadystatechange = function()
		{

			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					var element =  document.getElementById('suppr'); //On prend l'élément 'suppr'
					if (typeof(element) != 'undefined' && element != null) //Si il est present sur la page
					{
						//On le modifie de façon à ajouter un élément au panier
  						// Exists.
						//document.getElementById('suppr').href ="index.php?action=bien&idb="+idbien+"/ajouter()";
						document.getElementById('suppr').setAttribute('ONCLICK','ajouter()');
						document.getElementById("suppr").textContent = "Ajouter au panier";
						document.getElementById('suppr').className = "btn btn-primary";
					} else {

						//Sinon c'est le bouton 'ajout' qui est présent sur la page et on le modifie de façon à ajouter au panier
						document.getElementById('ajout').setAttribute('ONCLICK','ajouter()');
						document.getElementById("ajout").textContent = "Ajouter au panier";
						document.getElementById('ajout').className = "btn btn-primary";
					}
                    
                    alert("Suppression du bien de votre panier réalisé avec succès!");

					

				}
				else
				{
					alert(value="Error: returned status code " + req.status + " " + req.statusText);
				}
			}
		};
		req.open("GET", "index.php?action=supprimerPanier&idb="+idbien, true);
		req.send(null);
	}

    function ajouter()
	{
		var req = null;
		if (window.XMLHttpRequest)
		{
			req = new XMLHttpRequest();
		}
		else if (window.ActiveXObject)
		{
			try {
				req = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e)
			{
				try {
					req = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}


		req.onreadystatechange = function()
		{
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{

					var element =  document.getElementById('ajout'); //On prend l'élément 'ajout'
					if (typeof(element) != 'undefined' && element != null) //Si il est present sur la page
					{
                    	//On le modifie de façon à supprimer un élément au panier
						//document.getElementById('ajout').href ="index.php?action=bien&idb="+idbien+"/supprimer()";
						document.getElementById('ajout').setAttribute('ONCLICK','supprimer()');
						document.getElementById("ajout").textContent = "Supprimer du panier";
						document.getElementById('ajout').className = "btn btn-danger";
					} else {
						//Sinon c'est le bouton 'suppr' qui est présent sur la page et on le modifie de façon à supprimer du panier
						document.getElementById('suppr').setAttribute('ONCLICK','supprimer()');
						document.getElementById("suppr").textContent = "Supprimer du panier";
						document.getElementById('suppr').className = "btn btn-danger";

					}
                    alert("Ajout du bien dans votre panier réalisé avec succès!");
				}
				else
				{
					alert(value="Error: returned status code " + req.status + " " + req.statusText);
				}
			}
		};
		req.open("GET", "index.php?action=ajouterPanier&idb="+idbien, true);
		req.send(null);
	}
</script>

        <div class="container page-bien">
            <div class="row align-items-start">
                <div class="bien-info col">

				<a id ="btnImprime" onclick="printPdf('index.php?action=bien&idb=<?php echo $bien['idb']; ?>')">
					<img src="images/imprime.png" width="50" height="50">
				</a>

			<script>
				//Script pour imprimer un bienen pdf
				printPdf = function (pdfUrl) {
					//On désactive les éléments que l'on ne souhaite pas voir sur le pdf
					document.getElementById("btnImprime").style.display = "none";
					document.getElementById("listBtn").style.display = "none";
					document.getElementById("btnAjoutPhoto").style.display = "none";
					//Pour chaque éléments ayant la class 'btnSuppPhoto', on la cache
					var btnSuppPhoto = document.getElementsByClassName("btnSuppPhoto");
					var i;
					for (i = 0; i < btnSuppPhoto.length; i++) {
    					btnSuppPhoto[i].style.display = 'none';
					}
					document.getElementById("footer").style.display = "none";

					window.print();

					//On réactive les éléments cachés
					document.getElementById("btnImprime").style.display = "block";
					document.getElementById("listBtn").style.display = "block";
					document.getElementById("btnAjoutPhoto").style.display = "block";
					//Pour chaque éléments ayant la class 'btnSuppPhoto', on la réactive
					var btnSuppPhoto = document.getElementsByClassName("btnSuppPhoto");
					var i;
					for (i = 0; i < btnSuppPhoto.length; i++) {
    					btnSuppPhoto[i].style.display = 'block';
					}
					document.getElementById("footer").style.display = "block";
				}
			</script>

                    <h2 class="display-4">
                        <?= htmlspecialchars($bien['typeb']) ?>
                        <em>en <?= $bien['typer'] ?></em>
                    </h2>

                    <h3>
                        <?= htmlspecialchars($bien['prix']) ?> €
                    </h3>

                    <p> Bien n°
                        <em id="idbien"> <?= nl2br(htmlspecialchars($bien['idb'])) ?></em>
                         de l'agence.
                    </p>

                    <p>
                        <?= nl2br(htmlspecialchars($bien['localisation'])) ?>
                    </p>
            
                    <p>
                        <?= nl2br(htmlspecialchars($bien['description'])) ?>
                    </p>

					<div id="listBtn">

                    <?php 
                        if(isset($_SESSION['login'])) : 
                            $p = new Panier();
                            if (!$p->estDansPanier($_SESSION['login'], $bien['idb'])) : ?> <!-- Si l'utilisateur n'a pas déjà le bien dans son panier, on lui propose de l'ajouter via un bouton-->                               
                                <a id="ajout" class="btn btn-primary" width=100% ONCLICK="ajouter()">Ajouter au panier</a>
                               
                            <?php
                            else : ?> <!-- Sinon on lui propose de le supprimer de son panier-->
                                <a id="suppr" class="btn btn-danger" width=100% ONCLICK="supprimer()">Supprimer du panier</a>
                            <?php 
                            endif;

                         endif; 
					?>

					<?php
						if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
							$u = new Utilisateur(); //On crée un utilisateur
							if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
					?>
								<hr>
								<a id="btnModif" class="btn btn-primary" width=100% href="index.php?action=modifierBien&amp;idb=<?= $bien['idb'] ?>">Modifier le bien</a>
								<hr>
								<p class="lead">Attention, cette action est irréversible.</p>
								<a id="btnSuppBien" class="btn btn-danger" width=100% href="index.php?action=supprimerBien&amp;idb=<?= $bien['idb'] ?>">Supprimer le bien</a>
					<?php
							}
						}
					?>

					<hr>
                    <p><a href="index.php?action=catalogue">Retour à la liste des biens</a></p>
					</div>

                </div>

                <div class="bien-image col">
                    <h2 class="display-4">Photos</h2>

					<div id="btnAjoutPhoto">
						<?php
							if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
								if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
						?>
								<hr>
								<a class="btn btn-primary" width=100% href="index.php?action=ajouterPhoto&amp;idb=<?= $bien['idb'] ?>">Ajouter une photo</a>
								<hr>
						<?php
								}
							}
						?>
					</div>

                    <?php
                        while ($photo = $photos->fetch())
                        {
                    ?>
                        <img src="<?= $photo['lien'] ?>" alt="<?= $photo['nom'] ?>" width="500" height="350">
                        <p><strong><?= htmlspecialchars($photo['nom']) ?></strong></p>
						
					<div class="btnSuppPhoto">

						<?php
							if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
								if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
						?>
									<a class="btn btn-danger" width=100% href="index.php?action=supprimerPhoto&amp;idb=<?= $bien['idb'] ?>&amp;idphoto=<?= $photo['idphoto'] ?>">Supprimer la photo</a>
									<hr>
						<?php
								}
							}
						?>
					</div>
						<?php
                        }
                    	?>

					

                </div>
            </div>
        </div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>