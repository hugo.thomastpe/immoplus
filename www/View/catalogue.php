<?php $title = 'Catalogue'; ?>

<?php ob_start(); ?>

    <div class="container white catalogue z-depth-2 col-10 top">

        <h1
        class="display-3">Notre catalogue</h1>

        <?php 
        require_once("View/recherche.php");
         ?>
 
            <?php
			    if(isset($_SESSION['login'])){ //Si il y a une variable de Session login
                    $u = new Utilisateur();
					if($u->isAdmin($_SESSION['login'])){ //On test si l'utilisateur est admin
			?>
						<a class="btn btn-primary" width=100% href="index.php?action=ajouterBien">Ajouter un bien</a>
						<hr>
			<?php
					}
				}
            ?>

        <div class="catalogue-liste row">

            <?php
                while ($data = $biens->fetch()) //Pour chaque bien
                {
                    $p=new Photo();
                    $photo=$p->getPhotoBien($data['idb']); //On prépare la requête pour récupérer la première photo du bien
                    $pic=$photo->fetch(); //On lance la requête et récupère le résultat dans la variable pic
                    //print_r($pic);

            ?>
            
            <div class="bien container col-3 row bien-catalogue">
                
                <img src="<?= $pic['lien'] ?>" alt="<?= $pic['nom'] ?>" width="250" height="200"> <!--On affiche la première image-->

                <article class="contenu col-12">
                    <h3>
                        <?= htmlspecialchars($data['typeb']) ?> - <?= htmlspecialchars($data['typer']) ?><!--On affiche la type de bien et le type de recherche-->
                    </h3>
                
                    <p> <!--On affiche la descrition et un lien pour accéder à la page complète du bien-->
                        <?= 
                            //nl2br : prend en compte les retour à la ligne et les traduit en balise html
                            //htmlspecialchars : prend en compte les caractères spéciaux et les traduis en entités html
                            nl2br(htmlspecialchars(substr($data['description'], 0, 50))) 
                        ?>
                        ...<br />
                        <em><a class="btn btn-primary" width=100% href="index.php?action=bien&amp;idb=<?= $data['idb'] ?>">Voir le bien</a></em>
                    </p>
                </article>
            </div>

            <?php
                }
                $biens->closeCursor();
            ?>

        </div>
    </div>

<?php $content = ob_get_clean();?> 

<!--Tout le code écrit entre ob_start() (ligne 3) et ob_get_clean() est inclus dans la variable content. -->
<!-- Cela permet d'intégrer tout cela au fichier common/template.php et d'afficher la page comme il faut. -->

<!-- une autre solution aurait été de créer deux vues header et footer mais cette solution est moins flexible.-->

<?php require('template.php'); ?>