<?php $title = 'Notre agence'; ?>

<?php ob_start(); ?>

<div class="container catalogue profile-page">
            <section class="description">
				<div class="row marg-top">
					<div class="col-12 col-md-6 agence-image1">
                        <!--L'appel de l'image est dans le css-->
                    </div>

					<div class="col-12 col-md-6 descriptiontext">
						<h2 class="display-3">Notre agence</h2>

						<div class="textmargin">
							<p>ImmoPlus, réseau n°1 en signature de viager. C’est plus de 100 ans d’expertise, d’innovation et de progrès dans l’immobilier au service de nos clients.</p>
							<p>Nous &oelig;uvrons tout les jours pour faire des projets de nos clients une réalité immobilière.</p>
							<p>Agilité, innovation, bienveillance et proximité autant de valeurs qui nous permettent au quotidien d’agir pour le bien des projets immobiliers de nos clients.</p>
							<p>Nous sommes présents sur l’ensemble des segments de l’immobilier et nous accompagnons ainsi nos clients de A à Z quel que soit leur parcours de vie.</p>
						</div>

					</div>
				</div>

			</section>

            <section class="description">
				<div class="row marg-top">

					<div class="col-12 col-md-6 descriptiontext">
						<h2 class="display-3">Quelques chiffres</h2>

						<div>
							<p>ImmoPlus c'est aussi : </p>
                            <ul>
                                <li class="lead"><?= $nbBiens ?> Biens proposés</li>
                                <li class="lead"><?= $nbUtili ?> Utilisateurs</li>
                                <li class="lead"><?= $nbCollab ?> Collaborateurs</li>
                                <li class="lead"><?= $nbPart ?> Partenaires</li>
                            </ul>
							
						</div>

					</div>

                    <div class="col-12 col-md-6 agence-image2">
                        <!--L'appel de l'image est dans le css-->
                    </div>

				</div>

			</section>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>