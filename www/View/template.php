<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> <?= $title ?> </title>
        <link rel="stylesheet" href="assets/style.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <meta name="description" content="ImmoPlus - Agence immobilière depuis plus de 100 ans et n°1 dans la vente de viager.">
        <meta name="keywords" content="Immoplus, ImmoPlus, achat maison, location appartement, immeubles, ascenseur, terrain avec de l'herbe constructible, ">
        <link rel="shortcut icon" href="images/favicon.ico"/>
    </head>
        
    <body>

        <header>
            <?php $u = new Utilisateur()?>
            <nav class="navbar navbar-expand-sm fixed-top"><!--bg-dark navbar-dark-->

                <a class="navbar-brand" href="index.php"><img src="images/logo_petit.png" width="75" height="60"></a>
                <a class="navbar-brand" href="index.php">Accueil</a>
                <a class="navbar-brand" href="index.php?action=catalogue">Catalogue</a>
                <a class="navbar-brand" href="index.php?action=partenaire">Partenaires</a>
                <a class="navbar-brand" href="index.php?action=collaborateur">Collaborateurs</a>
                <a class="navbar-brand" href="index.php?action=agence">Notre Agence</a>
                <a class="navbar-brand" href="index.php?action=contact">Contact</a>
                <?php if(isset($_SESSION['login'])) { ?>
                    <?php if($u->isAdmin($_SESSION['login'])) { ?>
                        <a class="navbar-brand" href="index.php?action=utilisateurs">Utilisateurs</a>
                    <?php } ?>
                    <a class="navbar-brand" href="index.php?action=panier">Panier</a>
                    <a class="navbar-brand" href="index.php?action=deconnexion">Déconnexion</a>
                    
                <?php }else{ ?>
                    <a class="navbar-brand" href="index.php?action=connexion">Connexion</a>
                    <a class="navbar-brand" href="index.php?action=inscription">Inscription</a>
                <?php } ?>
            </nav>
        </header>

        <?= $content ?>

        <div>
            <footer class="col-12 text-align" id="footer">
                <p>&copy; Site créé par <a href="https://hugo-thomas.com/" target="_blank">Hugo THOMAS</a> et Mintonou ABATAN en 2021</p>
            </footer>
        </div>
    
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>