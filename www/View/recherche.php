<script language="JavaScript">

  function rechercherBien()
  { 
    var req = null; 
    if (window.XMLHttpRequest)
    {
      req = new XMLHttpRequest();

    } 
    else if (window.ActiveXObject) 
    {
      try {
        req = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e)
      {
        try {
          req = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {}
      }
          }
    req.onreadystatechange = function()
    { 

      if(req.readyState == 4)
      {
        if(req.status == 200)
        {
          document.getElementById('lareponse').innerHTML=req.responseText;  
        } 
        else  
        {
          document.getElementById('lareponse').value="Error: returned status code " + req.status + " " + req.statusText;
        } 
      } 
    }; 
    req.open("GET","ajax_search.php&localite="+document.recherche.localite.value+"&typSearch="+document.recherche.typSearch+"&typBien="+document.recherche.typBien+"&prixMin="+document.recherche.PrixMin.value+"&prixMax="+document.recherche.PrixMax.value,true); 
    
    req.send(null); 
  } 
  </script>
    <div class="container-fluid mt-5 px-5 jumbotron">
        <form 
          name="recherche" 
          method="POST" 
          class="px-5"
          
        ><!--action="index.php?action=rechercher"-->
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="localite">Localisation</label>
                <input type="text" require placeholder="Code postale" class="form-control" id="localite" name="localite">
              </div>
              <div class="form-group col-md-2">
                <label for="typSearch">Type de recherche</label>
                <select id="typSearch" name="typSearch" class="" require>
                  <option selected>Vente</option>
                  <option>Location</option>
                  <option>Autre</option>
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="typBien">Type de bien</label>
                <select id="typBien" name="typBien" class="" require>
                  <option selected>Maison</option>
                  <option>Appartement</option>
                  <option>Bureau</option>
                  <option>Garage</option>
                  <option>Terrain</option>
                  <option>Autre</option>
                </select>
              </div>
              <div class="form-group col-md-4 ">
                <label for="inputZip">Prix</label>
                <div data-role="rangeslider" class="pt-2">
                  <input type="number" require value="" placeholder="Prix minimum" step="10" min="0" max="3000000" id="prixMin" name="prixMin">
                  <input type="number" require value="" placeholder="Prix maximum" step="10" min="0" max="3000000" id="prixMax" name="prixMax">
                </div>
              </div>
              <div class="form-group col-md-2 pt-4">
                <input id="submit" type="submit" value="Rechercher" class="btn btn-primary" name="" ONCLICK="rechercherBien()" />
              </div>
            </div>
        </form>
        <p id="lareponse">...</p>
    </div>
    <div>
      <?php if (isset($biensRech)){
        foreach  ($biensRech as $bien) {
          echo "<option value='".$bien["idb"]."'>".$bien["typeb"]."</option>";
        }
      }
      ?>
    </div>

            <div class="bien container col-3 row bien-catalogue">
                
                <img src="ici je veux afficher l'image qui ressort de la recherche" alt="face" width="250" height="200">                <article class="contenu col-12">
                    <h3>
                        ici le nom qui sort du résultat de la fonction recherche
                    </h3>
                
                    <p> <!--On affiche la descrition et un lien pour accéder à la page complète du bien-->
                                                <br>
                        <em><a class="btn btn-primary" width="100%" href="index.php?action=bien&amp;idb=1">Voir le bien</a></em>
                    </p>
                </article>
            </div>

                        
          