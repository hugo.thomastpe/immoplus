<?php

session_start();
require('Controller/Controller.class.php');
require('Controller/ControllerBien.class.php');
require('Controller/ControllerPartenaire.class.php');
require('Controller/ControllerCollaborateur.class.php');
require('Controller/ControllerPhoto.class.php');
require('Controller/ControllerUtilisateur.class.php');
require('Controller/ControllerPanier.class.php');


$controllerGeneral = new Controller();
$controllerBien = new ControllerBien();
$controllerPartenaire = new ControllerPartenaire();
$controllerCollaborateur = new ControllerCollaborateur();
$controllerPhoto = new ControllerPhoto();
$controllerUtilisateur = new ControllerUtilisateur();
$controllerPanier = new ControllerPanier();



try{
    if (isset($_GET['action'])) {
        
        //Accueil
        if($_GET['action'] == 'accueil') {
            $controllerGeneral->accueil();
        }

        //Notre agence
        if($_GET['action'] == 'agence') {
            $controllerGeneral->agence();
        }
        
        //Catalogue
        elseif ($_GET['action'] == 'catalogue') {
            $controllerGeneral->catalogue();
        }

        //Rechercher
        elseif ($_GET['action'] == 'rechercher') {
            //require("View/ajax_search.php");
            $controllerGeneral->rechercher();
        }

        //Contact
        elseif ($_GET['action'] == 'contact') {
            $controllerGeneral->contact();
        }

        //Collaborateur
        elseif ($_GET['action'] == 'collaborateur') {
            $controllerCollaborateur->collaborateur();
        }

        //Ajouter un collaborateur
        elseif ($_GET['action'] == 'ajouterCollab') {
            $controllerCollaborateur->ajouterCollab();
        }

        //Supprimer un collaborateur de la liste
        elseif ($_GET['action'] == 'supprimerCollab') {
            if (isset($_GET['idc'])) { //On vérifie si l'id d'un collaborateur à été donné
                $controllerCollaborateur->supprimerCollab();
            }else{
                $controllerGeneral->error();
            }
        }

        //Partenaire
        elseif ($_GET['action'] == 'partenaire') {
            $controllerPartenaire->partenaire();
        }

        //Ajouter un partenaire
        elseif ($_GET['action'] == 'ajouterPart') {
            $controllerPartenaire->ajouterPart();
        }

        //Supprimer un partenaire de la liste
        elseif ($_GET['action'] == 'supprimerPart') {
            if (isset($_GET['idp'])) {
                $controllerPartenaire->supprimerPart();
            }else{
                $controllerGeneral->error();
            }
        }

        //Connexion
        elseif($_GET['action'] == 'connexion') {
            $controllerGeneral->connexion();
        }

        //Inscription
        elseif($_GET['action'] == 'inscription') {
            $controllerGeneral->inscription();
        }

        elseif ($_GET['action'] == 'panier') {
            $controllerPanier->panier();
        }

        elseif ($_GET['action'] == 'ajouterPanier') {
            if (isset($_GET['idb']) && $_GET['idb'] > 0) {
                $controllerPanier->ajouterAuPanier();
            }else{
                $controllerGeneral->error();
            }
        }

        elseif ($_GET['action'] == 'supprimerPanier') {
            if (isset($_GET['idb']) && $_GET['idb'] > 0) {
                $controllerPanier->supprimerDuPanier();
            }else{
                $controllerGeneral->error();
            }
        }

        //Liste des utilisateurs
        elseif($_GET['action'] == 'utilisateurs') {
            $controllerUtilisateur->utilisateurs();
        }

        //Supprimer un utilisateur de la liste
        elseif ($_GET['action'] == 'supprimerUtilisateur') {
            if (isset($_GET['idu'])) {
                $controllerUtilisateur->supprimerUtilisateur();
            }else{
                $controllerGeneral->error();
            }
        }

        
        
        //Déconnexion
        elseif($_GET['action'] == 'deconnexion') {
            $controllerGeneral->deconnexion();
        }

        //Afichage d'un bien
        elseif ($_GET['action'] == 'bien') {
            if (isset($_GET['idb']) && $_GET['idb'] > 0) { //Teste la présence de l'id d'un bien avant de l'afficher en appelant la fonction du controller
                $controllerBien->bien();
            }
            else {
                $controllerGeneral->error();
            }
        }

        //Ajouter un bien
        elseif ($_GET['action'] == 'ajouterBien') {
            $controllerBien->ajouterBien();
        }

        //Supprimer un bien de la liste
        elseif ($_GET['action'] == 'supprimerBien') {
            if (isset($_GET['idb'])) {
                $controllerBien->supprimerBien();
            }else{
                $controllerGeneral->error();
            }
        }

        //Modifier un bien
        elseif ($_GET['action'] == 'modifierBien') {
            if (isset($_GET['idb'])) {
                $controllerBien->modifierBien();
            }
            else{
                $controllerGeneral->error();
            }
        }

        //Supprimer la photo d'un bien
        elseif ($_GET['action'] == 'supprimerPhoto') {
            if (isset($_GET['idb'])) {
                if (isset($_GET['idphoto'])) {
                    $controllerPhoto->supprimerPhoto();
                } else{
                    $controllerGeneral->error();
                }
            }
            else{
                $controllerGeneral->error();
            }
        }

        //Ajouter une photo à un bien
        elseif ($_GET['action'] == 'ajouterPhoto') {
            if (isset($_GET['idb'])) {
                $controllerPhoto->addPhoto();
            }
            else{
                $controllerGeneral->error();
            }
        }
    }
    else {
        $controllerGeneral->accueil();
    }
} catch(Exception $e) { // S'il y a eu une erreur, alors...
    echo 'Erreur : ' . $e->getMessage();
    require("View/error/error.php");
}
