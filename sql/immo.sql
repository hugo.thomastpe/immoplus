-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : sam. 08 jan. 2022 à 11:49
-- Version du serveur :  10.5.12-MariaDB
-- Version de PHP : 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `immo`
--

-- --------------------------------------------------------

--
-- Structure de la table `_bien`
--

CREATE TABLE `_bien` (
  `idb` int(11) NOT NULL,
  `typer` varchar(20) NOT NULL,
  `typeb` varchar(20) NOT NULL,
  `prix` int(11) NOT NULL,
  `localisation` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_bien`
--

INSERT INTO `_bien` (`idb`, `typer`, `typeb`, `prix`, `localisation`, `description`) VALUES
(1, 'Vente', 'Maison', 262500, 'Quartier Rive Gauche à Lannion (22300)', 'Prix net vendeur\r\n6 pièces\r\n5 chambres\r\nCuisine équipée, salle de bain(baignoire) et toilettes.\r\nPièce à vivre : Séjour 21m², salle à manger et entrée séparée.\r\nChauffage au fioul\r\n110 m²\r\nTerrain 2215 m²\r\n\r\nDiagnostic de performance énergétique (DPE) : D\r\n\r\nIndice d\'émission de gaz à effet de serre (GES) : D'),
(2, 'Location', 'Appartement', 547, 'Quartier Atalante Beaulieu à Rennes (35000)', 'Prix CC\r\n2 pièces\r\n1 chambre\r\n38,7 m²\r\nÉtage 2/–\r\n\r\nExposé Sud\r\nParking et ascenseur compris\r\n\r\nDiagnostic de performance énergétique (DPE) : A\r\nIndice d\'émission de gaz à effet de serre (GES) : C'),
(3, 'Vente', 'Appartement', 276000, 'à Rennes (35000)', 'Prix net vendeur\r\n\r\n3 pièces\r\n2 chambres\r\n66 m²\r\nÉtage 2/–\r\nAscenseur\r\n\r\nToilettes séparées'),
(4, 'Vente', 'Parking', 29000, 'RENNES (35000)', 'Garage quartier Saint Hélier, 500 m de la gare. Dans un immeuble récent et sécurisé, garage fermé, 14 m² environ. Emplacement idéal à 500 m à pied de la gare, rue Saint Hélier / rue du Verger. Idéal pour des résidents ayant besoin de stockage ou de personnes souvent en déplacement via le TGV.'),
(5, 'Vente', 'Terrain', 395000, 'RENNES (35000)', 'Terrain RENNES - 891 m2. EXCLUSIVITE AMEPI - RENNES BELLANGERAIS\r\nRARE à la vente.\r\nTerrain constructible d\'environ 891 m² avec accès individuel et exposition Sud-Ouest sans vis à vis. Non Viabiilisé, Possibilité Gaz de ville, tout à l\'égout, proche des commerces, écoles, transports et de la rocade de RENNES.  \r\nLIBRE DE CONSTRUCTEUR. \r\nFrais d\'agence inclus.'),
(6, 'Autre', 'Maison', 246180, 'RENNES (35000)', 'VIAGER OCCUPÉ\r\nNOUVEAUTÉ ! RENNES - QUARTIER DE LA POTERIE À 500M DU MÉTRO\r\n\r\n- 700M DU CHU RENNES - Hôpital Sud - Maternité\r\n- 4KM de la gare TGV\r\n- 5KM de l\'hyper centre de Rennes et de la Place Sainte Anne\r\n- 11KM de l\'aéroport Rennes Bretagne\r\n\r\nÀ vendre, dans un lotissement calme, une maison de type 5 d\'environ 110m² habitables avec jardin de 216m².\r\n\r\nElle se compose au rez-de-chaussée d\'une entrée, une pièce de vie traversante, une cuisine aménagée, un garage et des W.-C.\r\nÀ l\'étage, un pallier desservant 4 chambres, une salle de bains et des W.-C.\r\n\r\nChauffage au gaz de ville + poêle à granulés.\r\n\r\nSurface : 111 m² - 5 pièces\r\nEquipements : Garage, parking\r\nÀ proximité : Collège, commerces, crêche, hopital, lycée, école, gare sncf, aeroport, bus, métro\r\nNbr. de niveaux : 1\r\nSurface de terrain : 216 m² ');

-- --------------------------------------------------------

--
-- Structure de la table `_collab`
--

CREATE TABLE `_collab` (
  `idc` varchar(20) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `poste` varchar(50) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_collab`
--

INSERT INTO `_collab` (`idc`, `nom`, `poste`, `photo`) VALUES
('HugoT', 'THOMAS Hugo', 'Webmaster', 'https://hugo-thomas.com/agritech/images/hugo.jpg'),
('ElonM', 'MUSK Elon', 'PDG', 'https://cdn.futura-sciences.com/buildsv6/images/largeoriginal/d/9/a/d9a1058910_50163142_elon-musk1.jpg'),
('MarcE', 'ETINGUE Marc', 'Marketing', 'https://images.radio-canada.ca/v1/ici-info/perso/yukon-aurore-boreale-rondel.JPG'),
('SarahC', 'CROCHE Sarah', 'Secrétaire', 'https://images.prismic.io/cadremploi-edito/2c42db4e-15ac-49a4-8d50-e4113a8adbb5_Bonne-photo-cv-femme1.JPG?auto=compress,format'),
('HarryC', 'COVERT Harry', 'Promoteur immobilier', 'https://studiobontant.fr/images/galeries/Big/1_photo_cv_linkedin_08.jpg'),
('AudeJ', 'JAVEL Aude', 'Promoteur immobilier', 'https://i2.wp.com/www.leptistudio.com/wp-content/uploads/2017/04/Photo-Cv-Professionnelle-Geneve-Divonne-les-Bains-6.jpg?fit=1024%2C1024&ssl=1'),
('JohnC', 'CENA John', 'catch les prix', 'https://m.media-amazon.com/images/I/51dqkoNGFJL._AC_.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `_panier`
--

CREATE TABLE `_panier` (
  `idu` varchar(20) NOT NULL,
  `idb` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_panier`
--

INSERT INTO `_panier` (`idu`, `idb`) VALUES
('admin', 2);

-- --------------------------------------------------------

--
-- Structure de la table `_partenaire`
--

CREATE TABLE `_partenaire` (
  `idp` varchar(20) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `logo` varchar(150) NOT NULL,
  `lien` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_partenaire`
--

INSERT INTO `_partenaire` (`idp`, `nom`, `logo`, `lien`) VALUES
('DunderMifflin', 'Dunder Mifflin', 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Dunder_Mifflin%2C_Inc.svg/1280px-Dunder_Mifflin%2C_Inc.svg.png', 'https://en.wikipedia.org/wiki/Dunder_Mifflin'),
('CCI', 'Chambre de Commerce et d Industrie', 'https://www.cci89.fr/wp-content/uploads/2019/01/Logo_cci.jpg', 'https://www.cci.fr/'),
('StephanePlaza', 'Stéphane Plaza Immobilier', 'https://www.stephaneplazaimmobilier.com/images/logo.svg', 'https://www.stephaneplazaimmobilier.com/');

-- --------------------------------------------------------

--
-- Structure de la table `_photo`
--

CREATE TABLE `_photo` (
  `idb` int(11) NOT NULL,
  `idphoto` varchar(20) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `lien` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_photo`
--

INSERT INTO `_photo` (`idb`, `idphoto`, `nom`, `lien`) VALUES
(1, 'Maison1.3', 'Salle à manger', 'https://v.seloger.com/s/width/800/visuels/0/j/1/q/0j1qtuexixwtkizifptjn2e2dg88sjen062zf4zgg.jpg'),
(1, 'Maison1.1', 'face', 'https://v.seloger.com/s/width/800/visuels/0/z/n/2/0zn2csdtixwtpyin6b8cjcwukzq900ympocphkgw0.jpg'),
(1, 'Maison1.2', 'cuisine', 'https://v.seloger.com/s/width/800/visuels/1/r/o/n/1ronh9vrp1fmzad8duookitzypbdyluzoqf3eyxds.jpg'),
(1, 'Maison1.4', 'Salon', 'https://v.seloger.com/s/width/800/visuels/2/a/a/g/2aagfborw7phk3hmgpctnrcrjj7a1ads9umi3ghhc.jpg'),
(1, 'Maison1.5', 'Salle de bain', 'https://v.seloger.com/s/width/800/visuels/2/3/r/z/23rz4l3fv7d6lkszg6ijf6abhpngam18mvbczp4sg.jpg'),
(1, 'Maison1.6', 'Chambre', 'https://v.seloger.com/s/width/800/visuels/1/4/7/b/147btdjmlmtzq63oxtgqrfm14exs3clr59b8tt474.jpg'),
(2, 'Appart1.1', 'Balcon', 'https://v.seloger.com/s/width/800/visuels/2/6/g/e/26geberxhmwh6quirxm1k0p336np0m3wzd44dm5uy.jpg'),
(2, 'Appart1.2', 'Salon', 'https://v.seloger.com/s/width/800/visuels/0/m/c/e/0mceiy1zv3fufitfp2trxu5gmz2dpbta8vynjeotm.jpg'),
(2, 'Appart1.3', 'Cuisine', 'https://v.seloger.com/s/width/800/visuels/1/o/b/1/1ob1igjfl3ky7egbrgzqu1pm9sktyf8plh71edvsa.jpg'),
(2, 'Appart1.4', 'Chambre', 'https://v.seloger.com/s/width/800/visuels/1/q/2/u/1q2uuy4sb7c4ed3sn8x1s4hymbcr4esydxhk3qp0q.jpg'),
(2, 'Appart1.5', 'Chambre 2', 'https://v.seloger.com/s/width/800/visuels/0/j/3/h/0j3h9ksnm5oz510saatfhvqohs662jiw4h6o000tm.jpg'),
(3, 'Appart2.1', 'Terrasse', 'https://v.seloger.com/s/width/800/visuels/1/m/q/4/1mq489rkfzckedazfrdgqt4taslunswl0tb5xv3b4.jpg'),
(3, 'Appart2.2', 'Salon', 'https://v.seloger.com/s/width/800/visuels/1/o/1/m/1o1m7500tbwxmrfokjziweek3xzmfug27vdqcblbm.jpg'),
(3, 'Appart2.3', 'Chambre', 'https://v.seloger.com/s/width/800/visuels/1/h/7/f/1h7fymnucch0clp172afpw4pjwskvjjvchkge224w.jpg'),
(4, 'Park1.1', 'Garage', 'https://mmf.logic-immo.com/mmf/ads/photo-prop-640x480/58b/b/bab34267-299f-4990-a8e4-29915ab44858.jpg'),
(5, 'Terrain1.1', 'Terrain 1', 'https://file.bienici.com/photo/immo-facile-37592413_media.immo-facile.com_office8_nidesia_vannes_catalog_images_pr_p_3_7_5_9_2_4_1_3_37592413a.jpg_DATEMAJ_05_01_2021-12_58_21?width=600&height=370&fit=cover'),
(5, 'Terrain1.2', 'Terrain 2', 'https://file.bienici.com/photo/immo-facile-37592413_media.immo-facile.com_office8_nidesia_vannes_catalog_images_pr_p_3_7_5_9_2_4_1_3_37592413b.jpg_DATEMAJ_05_01_2021-12_58_21?width=600&height=370&fit=cover'),
(6, 'Viager1.1', 'façade', 'https://cdn.costes-viager.com/ipx/400x300/422135077-9dd1eaddab61b0e1af5577f2-74235860.jpg'),
(6, 'Viager1.2', 'cuisine', 'https://cdn.costes-viager.com/ipx/400x300/422135077-dafa267ab261b0e1b1aebfe0-92945684.jpg'),
(6, 'Viager1.3', 'salon', 'https://cdn.costes-viager.com/ipx/400x300/422135077-81cd91979861b0e1b5203752-59038692.jpg'),
(6, 'Viager1.4', 'Chambre', 'https://cdn.costes-viager.com/ipx/400x300/422135077-d4d15fa89c61b0e1b26552d2-16191333.jpg'),
(6, 'Viager1.5', 'Bureau', 'https://cdn.costes-viager.com/ipx/400x300/422135077-e007eb41b061b0e1b312ffd5-51829319.jpg'),
(2, 'Appartement2.piscine', 'piscine', 'https://www.guide-piscine.fr/medias/image/piscine-lau-folies-a-lau-balagnas-2555-1200-800.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `_utilisateur`
--

CREATE TABLE `_utilisateur` (
  `idu` varchar(20) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `mdp` varchar(200) NOT NULL,
  `admin` varchar(4) NOT NULL DEFAULT 'non'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_utilisateur`
--

INSERT INTO `_utilisateur` (`idu`, `nom`, `prenom`, `mdp`, `admin`) VALUES
('a', 'a', 'a', '$2y$10$JT6VacBWllbxvG9eE7XP2eaT89oR4XMvkTlGlIIfwlS8mvyO8X12e', ''),
('admin', 'ADMIN', 'admin', '$2y$10$uElReDcAnGDaZGG9luWxnOmyet74nDoKz7O0FyacoVws3rP4C64NG', 'oui'),
('d', 'd', 'd', '$2y$10$JLTYWL38U8HB/QqJFlN96ugfZO4dQoGIxckX049AuNQ3xPQbnCR.a', ''),
('hugo', 'h', 'h', '$2y$10$XglBx9AXMthIB5YH3vgn7OhqZ2ARXFRqY07MJ0SN7QeIgP2RxqeSa', '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `_bien`
--
ALTER TABLE `_bien`
  ADD PRIMARY KEY (`idb`);

--
-- Index pour la table `_collab`
--
ALTER TABLE `_collab`
  ADD PRIMARY KEY (`idc`);

--
-- Index pour la table `_panier`
--
ALTER TABLE `_panier`
  ADD PRIMARY KEY (`idu`,`idb`);

--
-- Index pour la table `_partenaire`
--
ALTER TABLE `_partenaire`
  ADD PRIMARY KEY (`idp`);

--
-- Index pour la table `_photo`
--
ALTER TABLE `_photo`
  ADD PRIMARY KEY (`idphoto`) USING BTREE;

--
-- Index pour la table `_utilisateur`
--
ALTER TABLE `_utilisateur`
  ADD PRIMARY KEY (`idu`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `_bien`
--
ALTER TABLE `_bien`
  MODIFY `idb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
